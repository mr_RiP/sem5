#include <LPC23xx.H>                       /* Описание LPC23xx */

#define STARTER (1<<28)
#define BUTTON (1<<29)
#define INJECT (1<<27)
#define IGNITE (1<<26)

void delay(void) {

	unsigned int i;
	for (i=0;i<0xfffff;i++){}
}

int main (void) 
{
	unsigned int tick = 0;
//Конфигурировать функции входов/выходов порта 0 на модуль GPIO  	   
  PINSEL3 	= 0x00000000;
//IODIR1 - Регистр направления ввода вывода (1 - вывод; 0 - ввод)
  IODIR1 = 0x1C000000; /* P0.26..28 программируем на вывод, остальные на ввод */
//IOSET1 - Регистр установки порта (1 - установк; 0 - нет изменений)
  IOSET1 = 0x1C000000; /* Устанавливаем высокий уровень на выходах (гасим светодиоды) */
	
	IOCLR1 = STARTER | INJECT; // стартер и инжектор запущены
	delay();
  while (1) 
	{ 
		if (IOPIN1 & BUTTON) // кнопка нажата - стартер отключен
			IOCLR1 = STARTER;
			
		if (tick)
		{
			IOCLR1 = IGNITE;
			tick = 0;
		}
		else
		{
			IOCLR1 = INJECT;
			tick = 1;
		}	
		
		delay();
		
		IOSET1 = STARTER | INJECT | IGNITE;
	}
}



-- 1.	�������� ��������� � ���������� ��� ��� ����������
-- ������ ������� �����c���� � ��������� ��� ������
USE [Timetable]
GO

IF OBJECT_ID(N'dbo.SwapDays') IS NOT NULL
DROP PROCEDURE [dbo].[SwapDays]
GO

CREATE PROCEDURE [dbo].[SwapDays] (@day_a int, @day_b int)
AS
BEGIN
	IF (@day_a != @day_b) AND (@day_a BETWEEN 1 AND 7) AND (@day_b BETWEEN 1 AND 7)
	BEGIN
		DECLARE @Swap TABLE ([OldTimeId] int, [NewTimeId] int)
		INSERT @Swap ([OldTimeId], [NewTimeId])
		SELECT DISTINCT
			[Classes].[TimeId],
			(
				SELECT MIN([Id])
				FROM [TimeFrames] tf_in
				WHERE 
					tf_in.[Begin] = tf_out.[Begin] AND
					tf_in.[End] = tf_out.[End] AND
					tf_in.[Nominator] = tf_out.[Nominator] AND
					tf_in.[Denominator] = tf_out.[Denominator] AND
					(
						(tf_in.[Day] = @day_a AND tf_out.[Day] = @day_b) OR
						(tf_in.[Day] = @day_b AND tf_out.[Day] = @day_a)
					)
			)
		FROM [Classes] INNER JOIN [TimeFrames] tf_out ON [Classes].[TimeId] = tf_out.[Id]
		WHERE tf_out.[Day] = @day_a OR tf_out.[Day] = @day_b;

		UPDATE [Classes]
		SET [TimeId] = 
		(
			SELECT MIN([NewTimeId])
			FROM @Swap 
			WHERE [OldTimeId] = [TimeId]
		)
		WHERE [TimeId] IN
		(
			SELECT [OldTimeId]
			FROM @Swap
		)
	END
END;
GO

/*
-- �����

SELECT *
FROM [dbo].[GetTimetable] (DATEFROMPARTS(2016,10,19), DATEFROMPARTS(2016,9,1))
SELECT *
FROM [dbo].[GetTimetable] (DATEFROMPARTS(2016,10,20), DATEFROMPARTS(2016,9,1))
GO

EXECUTE [dbo].[SwapDays] 3, 4
GO
*/

-- 2.	����������� ��������� ��� ��������� � ����������� ���
-- ������� ������� ���������� �������� �������������� ��� ��������� �������
USE [Timetable]
GO

IF OBJECT_ID(N'dbo.GetDepartmentHierarchy') IS NOT NULL
DROP PROCEDURE [dbo].[GetDepartmentHierarchy]
GO

CREATE PROCEDURE [dbo].[GetDepartmentHierarchy] (@department_id int)
AS
BEGIN
	WITH [Hierarchy] AS
	(
		SELECT
			[Id],
			[FirstName], 
			[MiddleName],
			[LastName],
			[AcademicRank],
			1 AS [DepartmentRank]
		FROM [Teachers]
		WHERE [DepartmentId] = @department_id AND [SupervisorId] IS NULL
	
		UNION ALL
		SELECT 
			[Teachers].[Id] AS [Id],
			[Teachers].[FirstName] AS [FirstName],
			[Teachers].[MiddleName] AS [MiddleName],
			[Teachers].[LastName] AS [LastName],
			[Teachers].[AcademicRank] AS [AcademinRank],
			[Hierarchy].[DepartmentRank] + 1 AS [DepartmentRank]
		FROM [Teachers] INNER JOIN [Hierarchy] ON [Teachers].[SupervisorId] = [Hierarchy].[Id]
	)
	SELECT *
	FROM [Hierarchy]
	ORDER BY [DepartmentRank] ASC
END;
GO

/*
-- ����
EXEC [GetDepartmentHierarchy] 5
GO
EXEC [GetDepartmentHierarchy] 1
GO
*/

-- 3.	��������� � ��������
-- 

USE [Timetable]
GO

IF OBJECT_ID(N'dbo.GetCoursesStats') IS NOT NULL
DROP PROCEDURE [dbo].[GetCoursesStats]
GO

CREATE PROCEDURE [dbo].[GetCoursesStats]
AS
BEGIN
	DECLARE @Stats TABLE
	(
		[Id] int PRIMARY KEY,
		[Name] nvarchar(64),
		[Labs] int,
		[Lectures] int, 
		[Seminars] int, 
		[Other] int
	);
	
	DECLARE @ClassesCount TABLE ([CourseId] int, [Type] char, [Count] int);
	WITH [Temp] AS
	(
		SELECT
			[Classes].[CourseId] AS [CourseId],
			(
				CASE [Classes].[Type] 
				WHEN N'������������ ������' THEN 'a'
				WHEN N'������' THEN 'e'
				WHEN N'�������' THEN 's'
				ELSE 'o'
				END
			) AS [Type],
			CAST([TimeFrames].[Nominator] AS int) + CAST([TimeFrames].[Denominator] AS int) AS [Count]
		FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
	)
	INSERT @ClassesCount 
	SELECT
		[CourseId],
		[Type],
		SUM([Count]) AS [Count]
	FROM [Temp]
	GROUP BY [CourseId], [Type];
	
	DECLARE @id int, @name nvarchar(64), @labs int, @lectures int, @seminars int, @other int;
	DECLARE CoursesCursor CURSOR
	LOCAL
	FOR
		SELECT
			[Id],
			[Name]
		FROM [Courses];

	OPEN CoursesCursor;
	FETCH NEXT FROM CoursesCursor INTO @id, @name
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @labs =
		(
			SELECT ISNULL(MIN([Count]), 0)
			FROM @ClassesCount
			WHERE [CourseId] = @id AND [Type] = 'a'
		);

		SET @lectures = 
		(
			SELECT ISNULL(MIN([Count]), 0)
			FROM @ClassesCount
			WHERE [CourseId] = @id AND [Type] = 'e'
		);

		SET @seminars = 
		(
			SELECT ISNULL(MIN([Count]), 0)
			FROM @ClassesCount
			WHERE [CourseId] = @id AND [Type] = 's'
		);

		SET @other = 
		(
			SELECT ISNULL(MIN([Count]), 0)
			FROM @ClassesCount
			WHERE [CourseId] = @id AND [Type] = 'o'
		);

		INSERT @Stats ([Id], [Name], [Labs], [Lectures], [Seminars], [Other] )
		VALUES (@id, @name, @labs, @lectures, @seminars, @other)

		FETCH NEXT FROM CoursesCursor INTO @id, @name
	END
	CLOSE CoursesCursor;

	SELECT *
	FROM @Stats
END;
GO

/*
-- ����
EXEC [GetCoursesStats]
GO
*/

-- 4.	��������� ��� ������� � ����������
/* 
������� 17.
������� �������� ��������� � ������� ����������, ������� ������� ����� �������� ��������, ��������� � ���������� WITH RECOMPILE,
� ������ ������� �� ����� SQL ����������� ������, ���������� ���������� ���������. ��������� �������� ��������� ��������������. 
*/

USE [master]
GO

IF OBJECT_ID(N'dbo.GetRecompilingProcedures') IS NOT NULL
DROP PROCEDURE [dbo].[GetRecompilingProcedures]
GO

CREATE PROCEDURE [dbo].[GetRecompilingProcedures] (@max_count int)
WITH RECOMPILE AS
BEGIN
	SELECT TOP (@max_count) [name]
	FROM [sys].[objects]
	WHERE [object_id] IN
	(
		SELECT [object_id]
		FROM [sys].[sql_modules]
		WHERE [is_recompiled] = 1
	)
END;
GO

/*
-- ����
EXEC [GetRecompilingProcedures] 5
GO

SELECT TOP 1000 [name]
	FROM [sys].[objects]
	WHERE [object_id] IN
	(
		SELECT [object_id]
		FROM [sys].[sql_modules]
		WHERE [is_recompiled] = 1
	)
*/
USE [Timetable]
GO

-- 1.	��������� �������
-- ���������� ����� ��� ������

-- �������, ���� ��� �������
IF OBJECT_ID(N'dbo.DayNo') IS NOT NULL
DROP FUNCTION [dbo].[DayNo]
GO

-- ������� �������
CREATE FUNCTION [dbo].[DayNo] (@day nvarchar(16))
RETURNS int
WITH RETURNS NULL ON NULL INPUT
AS
BEGIN
	RETURN CASE @day
		WHEN N'�����������' THEN 1
		WHEN N'�����������' THEN 1
		WHEN N'�������' THEN 2
		WHEN N'�������' THEN 2
		WHEN N'�����' THEN 3
		WHEN N'�����' THEN 3
		WHEN N'�������' THEN 4
		WHEN N'�������' THEN 4
		WHEN N'�������' THEN 5
		WHEN N'�������' THEN 5
		WHEN N'�������' THEN 6
		WHEN N'�������' THEN 6
		WHEN N'�����������' THEN 7
		WHEN N'�����������' THEN 7
		ELSE NULL
	END
END;
GO

/*
-- �����
SELECT [dbo].[DayNo] (NULL)
SELECT [dbo].[DayNo] (N'�����������')
SELECT [dbo].[DayNo] (N'�������')
SELECT [dbo].[DayNo] (N'�������')
SELECT [dbo].[DayNo] (N'�������')
GO
*/

-- 2.	������������� ��������� �������
-- ���������� ������ �� Classes �������� ���������� ���, ���� ������ � ����

IF OBJECT_ID(N'dbo.GetClasses') IS NOT NULL
DROP FUNCTION [dbo].[GetClasses]
GO

CREATE FUNCTION [dbo].[GetClasses] (@day nvarchar(16), @week_type nvarchar(2), @date date)
RETURNS table
AS
	RETURN 
	(
		SELECT *
		FROM [Classes]
		WHERE (((@date IS NOT NULL) AND (@date BETWEEN [BeginDate] AND [EndDate])) OR (@date IS NULL)) AND
			[TimeId] IN 
			(
				SELECT [Id]
				FROM [TimeFrames]
				WHERE 
					[dbo].[DayNo](@day) = [Day] AND
					(
						((@week_type = N'��') AND ([Nominator] = 1)) OR 
						((@week_type = N'��') AND ([Denominator] = 1)) OR 
						(([Nominator] = 0) AND ([Denominator] = 0))
					)
			)
	);
GO		

/*
-- �����
SELECT *
FROM [dbo].[GetClasses] (N'�����������', N'��', GETDATE())
SELECT *
FROM [dbo].[GetClasses] (N'�����', N'��', GETDATE())
SELECT *
FROM [dbo].[GetClasses] (N'�������', N'��', GETDATE())
GO
*/

-- 3.	���������������� ��������� ������� 

IF OBJECT_ID(N'dbo.GetTimetable') IS NOT NULL
DROP FUNCTION [dbo].[GetTimetable]
GO

CREATE FUNCTION [dbo].[GetTimetable](@date date, @semester_begin_date date)
RETURNS @Timetable Table 
(
	[BeginTime] time(0) NOT NULL,
	[EndTime] time(0) NOT NULL,
	[Course] nvarchar(64) NOT NULL,
	[Type] nvarchar(32) NOT NULL,
	[Room] nvarchar(16) NOT NULL,
	[Teacher] nvarchar(max) NULL,
	[Department] nvarchar(max) NULL
)
AS
BEGIN
	DECLARE @day int = DATEPART(weekday, @date);
	DECLARE @week_type bit = DATEDIFF(week, @semester_begin_date, @date) % 2;
	INSERT INTO @Timetable ([BeginTime], [EndTime], [Course], [Type], [Room], [Teacher], [Department])
	SELECT 
		[TimeFrames].[Begin],
		[TimeFrames].[End],
		[Courses].[Name],
		[Classes].[Type],
		[Classes].[Room],
		CONCAT([Teachers].[LastName], N' ', [Teachers].[FirstName], N' ', [Teachers].[MiddleName]),
		CONCAT([Departments].[Faculty], N'-', [Departments].[Index])
	FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
		INNER JOIN [Teachers] ON [Classes].[TeacherId] = [Teachers].[Id]
		INNER JOIN [Departments] ON [Teachers].[DepartmentId] = [Departments].[Id]
		INNER JOIN [Courses] ON [Classes].[CourseId] = [Courses].[Id]
	WHERE 
		(@date BETWEEN [Classes].[BeginDate] AND [Classes].[EndDate]) AND
		(@day = [TimeFrames].[Day]) AND
		(
			((@week_type = 0) AND ([TimeFrames].[Nominator] = 1)) OR
			((@week_type = 1) AND ([TimeFrames].[Denominator] = 1))
		)
	RETURN;
END;
GO

/*
-- �����
SELECT *
FROM [dbo].[GetTimetable] (GETDATE(), DATEFROMPARTS(2016,9,1))
SELECT *
FROM [dbo].[GetTimetable] (DATEFROMPARTS(2016,10,20), DATEFROMPARTS(2016,9,1))
SELECT *
FROM [dbo].[GetTimetable] (DATEFROMPARTS(2016,10,10), DATEFROMPARTS(2016,9,1))
GO
*/

-- 4.	����������� �������
-- ���������� ���������� ������ �����

IF OBJECT_ID(N'dbo.Factorial') IS NOT NULL
DROP FUNCTION [dbo].[Factorial]
GO

CREATE FUNCTION [dbo].[Factorial] (@n int)
RETURNS int
WITH RETURNS NULL ON NULL INPUT
AS
BEGIN
	IF @n <= 0 RETURN NULL;
	IF @n = 1 RETURN 1;
	RETURN @n * [dbo].[Factorial] (@n-1);
END;
GO

/*
-- �����
SELECT [dbo].[Factorial] (NULL)
SELECT [dbo].[Factorial] (1)
SELECT [dbo].[Factorial] (3)
GO
*/
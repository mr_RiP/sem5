USE [Timetable]
GO

-- 1.	������� AFTER
-- ���������, ����� ������������ ������������� ������ ��� � ��� �������. ���� ��� �� ��� - �������� ���� ������������.

IF OBJECT_ID(N'TRG_TeacherTransferred') IS NOT NULL
DROP TRIGGER [TRG_TeacherTransferred]
GO

CREATE TRIGGER [TRG_TeacherTransferred] ON [Teachers]
AFTER UPDATE, INSERT
AS
BEGIN
	IF (@@ROWCOUNT > 0)
	BEGIN
		UPDATE [Teachers]
		SET [SupervisorId] = NULL
		WHERE [Id] IN
		(
			SELECT [Workers].[Id]
			FROM [Teachers] AS [Workers] INNER JOIN [Teachers] AS [Supervisors] ON [Workers].[SupervisorId] = [Supervisors].[Id]
			WHERE [Workers].[DepartmentId] != [Supervisors].[DepartmentId]
		)
	END
END;
GO

-- 2.	�������� INSTEAD OF
-- "������" �� �� ������ �� ������������� ��� ��� ��������

IF OBJECT_ID(N'TRG_DeleteTeacher') IS NOT NULL
DROP TRIGGER [TRG_DeleteTeacher]
GO

CREATE TRIGGER [TRG_DeleteTeacher] ON [Teachers]
INSTEAD OF DELETE
AS
BEGIN
	IF (SELECT COUNT(*) FROM [deleted]) > 0
	BEGIN
		-- ��� ������� ���������� ������������� ������ �������� ��� ������������ � �������� ������������ ��� �����������
		-- A ����� ���������� ����� ������������ � �������� ������������� ��� ��� �������, ������� ��� ��������� �������������
		DECLARE DelCursor CURSOR 
		LOCAL
		DYNAMIC
		FOR
			SELECT
				[Id],
				[SupervisorId]
			FROM [deleted];
		OPEN DelCursor;
		DECLARE @id int, @supervisor_id int
		FETCH NEXT FROM DelCursor INTO @id, @supervisor_id
		WHILE (@@FETCH_STATUS = 0)	
		BEGIN
			UPDATE [Teachers]
			SET [SupervisorId] = @supervisor_id
			WHERE [Id] = @id

			UPDATE [Classes]
			SET [TeacherId] = @supervisor_id
			WHERE [TeacherId] = @id

			FETCH NEXT FROM DelCursor INTO @id, @supervisor_id
		END
		CLOSE DelCursor;

		-- ������� ��������������
		DELETE FROM [Teachers]
		WHERE [Id] IN
		(
			SELECT [Id]
			FROM deleted
		)
	END
END;
GO	
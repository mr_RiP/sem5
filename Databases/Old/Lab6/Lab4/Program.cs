﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            var proc = new XmlProcessor();

            string filename = null;

            Action[] funcs = new Action[]
            {
                proc.Open, proc.Find, proc.Access, proc.Change, () => Environment.Exit(0)
            };

            while (true)
            {
                Console.WriteLine("1. Открыть документ из файла");
                Console.WriteLine("2. Поиск информации в документе");
                Console.WriteLine("3. Доступ к содержимому узлов");
                Console.WriteLine("4. Внесение изменений в документ");
                Console.WriteLine("5. Выход из программы");

                int code = Convert.ToInt32(Console.ReadLine());
                funcs[code - 1]();
            }
        }
    }
}

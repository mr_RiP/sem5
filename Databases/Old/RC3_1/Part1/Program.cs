﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Part1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Starting...\n");

                var doc = new XmlDocument();
                doc.Load("XMLFile1.xml");

                var books = doc.GetElementsByTagName("Book");
                foreach (XmlNode book in books)
                {
                    var date = DateTime.Parse(book["PublicationDate"].InnerText);
                    if (date.Year < 2009)
                    {
                        var newPrice = (double.Parse(book["Price"].InnerText) * 0.9).ToString();
                        book["Price"].InnerText = newPrice;
                        Console.WriteLine($"Book name: \"{book["Title"].InnerText}\", new price: {newPrice}");
                    }
                }
                doc.Save("result.xml");

                Console.WriteLine("\nFinished");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.ReadLine();
            }
            
        }
    }
}

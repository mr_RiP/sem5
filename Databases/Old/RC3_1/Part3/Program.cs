﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Part3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Starting...");

                var xdoc = XDocument.Load("XMLFile1.xml");

                var sourcePrices = xdoc.Elements()
                    .Elements("Book")
                    .Where(elem => DateTime.Parse(elem.Element("PublicationDate").Value).Year < 2009)
                    .Select(elem => elem.Element("Price"));
                foreach (var price in sourcePrices)
                    price.Value = (double.Parse(price.Value) * 0.9).ToString();

                xdoc.Save("result.xml");

                Console.WriteLine("Finished!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}

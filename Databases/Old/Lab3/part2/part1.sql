use dbGameServer
go

if exists (select * from sysobjects where id = object_id(N'dbo.FixSpaces'))
	drop procedure dbo.FixSpaces
go

create procedure dbo.FixSpaces
as
begin
	update dbo.[C]
	set Cname = ltrim (Cname)
	update dbo.[A]
	set Aname = ltrim (Aname)
	update dbo.[S]
	set Sname = ltrim (Sname)
end
go

update S set Sname = ' aaa' where Sno = 1
go
update S set Sname = '   bbb' where Sno = 2
go
update S set Sname = '        ccc' where Sno = 3
go

exec dbo.FixSpaces
go

select Sname from S
go
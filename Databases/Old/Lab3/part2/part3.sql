use dbGameServer
go

if exists (select * from sysobjects where id = object_id(N'dbo.GetData'))
	drop procedure dbo.GetData
go

create procedure dbo.GetData
as
begin
	declare @counter int, @name char(11), @level smallint, @money int
	declare _cursor cursor
	global
	for
	select Cname, Clvl, Cmoney from dbo.[C]
	open _cursor

	fetch next from _cursor into @name, @level, @money
	select @counter = 0

	while (@@FETCH_STATUS = 0 and @counter < 100)
	begin
		select @counter = @counter + 1
		if @level > 90
			print 'Character ' + rtrim (@name) + ' with level ' + cast(@level as char(3)) + 'has ' + rtrim(cast(@money as char(10))) + ' money'
		fetch next from _cursor into @name, @level, @money
	end
	close _cursor
	deallocate _cursor
end
go

dbo.GetData
go
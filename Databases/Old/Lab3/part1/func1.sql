if exists (select * from sysobjects where id = object_id(N'dbo.CalculateSphereVolume'))
	drop function dbo.CalculateSphereVolume
go


create function dbo.CalculateSphereVolume (@radius float = 1.0)
returns float
with returns null on null input
as
begin
	return PI() * POWER(@radius, 3) * 4 / 3
end;
go

select dbo.CalculateSphereVolume(10)
select dbo.CalculateSphereVolume(null)
select dbo.CalculateSphereVolume(2.5)
go
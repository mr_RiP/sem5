if exists (select * from sysobjects where id = object_id(N'dbo.GetThreePowersSum'))
	drop function dbo.GetThreePowersSum
go

create function dbo.GetThreePowersSum (@val int)
returns int
with returns null on null input
as
begin
	declare @result int;
	declare @absVal int;
	set @absVal = abs (@val);
	with Powers (Num) as
	(
		select 1
		union all
		select Num * 3
		from Powers
		where Num < @absVal
	)
	select @result = sum(Num) from Powers
	return @result;
end
go

select dbo.GetThreePowersSum(27)
select dbo.GetThreePowersSum(-100)
select dbo.GetThreePowersSum(null)
select dbo.GetThreePowersSum(281)
go
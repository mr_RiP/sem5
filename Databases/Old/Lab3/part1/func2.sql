if exists (select * from sysobjects where id = object_id(N'dbo.Dividers'))
	drop table dbo.Dividers

if exists (select * from sysobjects where id = object_id(N'dbo.GetDigits'))
	drop function dbo.GetDigits
go

create table dbo.Dividers (Div int not null primary key)
go

with DivFill (Div) as
(
	select 10
	union all
	select Div * 10
	from DivFill
	where Div < 1000000000
)
insert into dbo.Dividers(Div)
select Div
from DivFill
go

create function dbo.GetDigits (@val int = 1)
returns table
as
return
(
	with Digits (div, res)
	as
	(
		select div, (abs(@val % div) / (div / 10)) as res
		from dbo.Dividers
		where (@val / (div / 10)) <> 0
	)
	select row_number() over (order by div desc) as Pos, res as Result from Digits
)
go

select * from dbo.GetDigits(-123)
select * from dbo.GetDigits(123456789)
select * from dbo.GetDigits(220000000)
go
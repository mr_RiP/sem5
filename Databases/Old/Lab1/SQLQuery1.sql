/* �������� �������� � ��������� ��������� ���� ������ [master] */ 
USE [master]
GO  

/* ���� ���� ������ [dbSPJ] ��� ����������, ���������� �� */
IF EXISTS (SELECT name FROM sys.databases WHERE name = 'dbSPJ')
DROP DATABASE [dbSPJ]
GO  


/* ������� ���� ������ [dbSPJ] */ 
CREATE DATABASE [dbSPJ]
GO 

/* ��������� � �������� ��������� ���� ������ [dbSPJ] */
USE [dbSPJ]
GO

/* ������� ������� ����������� [S] */
CREATE TABLE [dbo].[S](
[Sno] [int] IDENTITY(1,1) NOT NULL,
[Sname] [varchar](20) NOT NULL,
[Status] [smallint] NULL,
[City] [varchar](15) NULL )
GO 
 
/* ������� ������� ������� [P] */ 
CREATE TABLE [dbo].[P]( 
[Pno] [int] IDENTITY(1,1) NOT NULL, 
[Pname] [varchar](20) NOT NULL, 
[Color] [char](10) NULL, 
[Weight] [real] NULL, 
[City] [varchar](15) NULL ) 
GO  

CREATE TABLE [dbo].[J]( 
[Jno] [int] IDENTITY(1,1) NOT NULL, 
[Jname] [varchar](20) NOT NULL, 
[City] [varchar](15) NULL ) 
GO  

CREATE TABLE [dbo].[SPJ]( 
[Sno] [int] NOT NULL, 
[Pno] [int] NOT NULL, 
[Jno] [int] NOT NULL, 
[Qty] [int] NULL ) 
GO

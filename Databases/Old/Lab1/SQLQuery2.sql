/* �������� �������� � ��������� ��������� ���� ������ [dbSPJ] */
USE [dbSPJ]
GO  

/* �������� ����������� ������� ����������� [S] ����� ���������� ����������� ���������� ����� 
� ����� ������������ */
ALTER TABLE [dbo].[S] ADD 
CONSTRAINT [PK_S] PRIMARY KEY ([Sno]), 
CONSTRAINT [UK_S] UNIQUE ([Sname]) 
GO 

/* �������� ����������� ������� ������� [P] ����� ���������� ����������� ���������� ����� */ 
ALTER TABLE [dbo].[P] ADD  
CONSTRAINT [PK_P] PRIMARY KEY ([Pno]) 
GO  

/* �������� ����������� ������� �������� [J] ����� ���������� ����������� ���������� ����� 
� ����� ������������ */ 
ALTER TABLE [dbo].[J] ADD  
CONSTRAINT [PK_J] PRIMARY KEY ([Jno]), 
CONSTRAINT [UK_J] UNIQUE ([Jname]) 
GO  

/* �������� ����������� ������� �������� [SPJ] ����� ���������� ����������� ���������� ����� 
� ������� ������ */ 
ALTER TABLE [dbo].[SPJ] ADD  
CONSTRAINT [PK_SP] PRIMARY KEY ( [Sno], [Pno], [Jno] ), 
CONSTRAINT [FK_SP_J] FOREIGN KEY([Jno]) REFERENCES [dbo].[J] ([Jno]) , 
CONSTRAINT [FK_SP_P] FOREIGN KEY([Pno]) REFERENCES [dbo].[P] ([Pno]) , 
CONSTRAINT [FK_SP_S] FOREIGN KEY([Sno]) REFERENCES [dbo].[S] ([Sno]) 
GO  

/* �������� ����������� ������ [S], [P] � [SPJ] ����� ���������� ����������� CHECK */ 
ALTER TABLE [dbo].[S] ADD 
CONSTRAINT [Status_chk] CHECK ([Status] BETWEEN 0 AND 100) 
GO  

ALTER TABLE [dbo].[P] ADD 
CONSTRAINT [Weight_chk] CHECK ([Weight] >= 0) 
GO  

ALTER TABLE [dbo].[P] ADD 
CONSTRAINT [Color_chk] CHECK (([Color]='�������' OR [Color]='�������' OR [Color]='�����'))
GO  

ALTER TABLE [dbo].[SPJ] ADD
CONSTRAINT [Qty_chk] CHECK ([Qty] >= 0)
GO
 
/* ������� ������� ��� City � ����������� ������� � ����� City ������ [S], [P] � [J] */ 
CREATE RULE [dbo].[City_rule]  AS 
@city IN ('��������', '��������', '������', '�����', '����', '������', '���������') 
GO

EXEC sp_bindrule 'City_rule', 'dbo.S.City' 
EXEC sp_bindrule 'City_rule', 'dbo.P.City' 
EXEC sp_bindrule 'City_rule', 'dbo.J.City' 
GO 
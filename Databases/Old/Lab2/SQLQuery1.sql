USE [master]
GO

USE [dbGameServer]
GO

-- 1. ������ � ���������� ���������
SELECT *
FROM C
WHERE Clvl < 20
ORDER BY Clvl DESC
GO

-- 2. ������ � ���������� BETWEEN
SELECT *
FROM C
WHERE Cmoney BETWEEN 10000 AND 50000
GO

-- 3. ������ � ���������� LIKE
SELECT *
FROM C
WHERE Cname LIKE '%pro%'
GO

-- 4. IN � ��������� �����������
WITH HiLvl AS
(
	SELECT Cno AS CharNo
	FROM C
	WHERE Clvl > 60
)
SELECT DISTINCT *
FROM A
WHERE Ano IN
(
	SELECT Ano
	FROM CAS JOIN HiLvl ON CAS.Cno = HiLvl.CharNo
)
GO

-- 5. EXISTS c ��������� �����������
SELECT *
FROM 
USE [Timetable]
GO

DELETE FROM [Classes]
GO
DBCC CHECKIDENT ('[Classes]', RESEED, 0);
GO

DELETE FROM [Teachers]
GO
DBCC CHECKIDENT ('[Teachers]', RESEED, 0);
GO

DELETE FROM [Courses]
GO
DBCC CHECKIDENT ('[Courses]', RESEED, 0);
GO

DELETE FROM [TimeFrames]
GO
DBCC CHECKIDENT ('[TimeFrames]', RESEED, 0);
GO

DELETE FROM [Departments]
GO
DBCC CHECKIDENT ('[Departments]', RESEED, 0);
GO

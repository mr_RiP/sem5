﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFiller
{
    class TeachersGenerator
    {
        public TeachersGenerator()
        {
            GenerateActual();
        }

        public void GenerateActual()
        {
            Data = new List<TeachersData>
            {
                new TeachersData { Id = 1, FirstName = "Игорь", MiddleName = "Владимирович", LastName = "Рудаков", DepartmentId = 1, AcademicRank = "доцент", },
                new TeachersData { Id = 2, FirstName = "Валерий", MiddleName = "Николаевич", LastName = "Ремарчук", DepartmentId = 5, AcademicRank = "профессор" },
                new TeachersData { Id = 3, FirstName = "Евгений", MiddleName = "Алексеевич", LastName = "Просуков", DepartmentId = 1, AcademicRank = "доцент", SupervisorId = 1 },
                new TeachersData { Id = 4, FirstName = "Михаил", MiddleName = "Васильевич", LastName = "Ульянов", DepartmentId = 1 , AcademicRank = "профессор", Email = "muljanov@mail.ru", SupervisorId = 1 },
                new TeachersData { Id = 5, FirstName = "Лидия", MiddleName = "Леонидовна", LastName = "Волкова", DepartmentId = 1, AcademicRank = "доцент", Email = "lvolkova@hse.ru", SupervisorId = 1 },
                new TeachersData { Id = 6, FirstName = "Наталья", MiddleName = "Юрьевна", LastName = "Рязанова", DepartmentId = 1, AcademicRank = "доцент", Email = "ryaz_nu@mail.ru", SupervisorId = 1 },
                new TeachersData { Id = 7, FirstName = "Павел", MiddleName = "Александрович", LastName = "Власов", DepartmentId = 3, AcademicRank = "доцент", Email = "pvlx@mail.ru" },
                new TeachersData { Id = 8, FirstName = "Юрий", MiddleName = "Владимирович", LastName = "Синчук", DepartmentId = 5, AcademicRank = "профессор", SupervisorId = 2 },
                new TeachersData { Id = 9, FirstName = "Андрей", MiddleName = "Владимирович", LastName = "Куров", DepartmentId = 1, AcademicRank = "доцент", SupervisorId = 1 },
                new TeachersData { Id = 10, FirstName = "Алексей", MiddleName = "Юрьевич", LastName = "Попов", DepartmentId = 2, AcademicRank = "доцент", Email = "alexpopov@bmstu.ru" },
                new TeachersData { Id = 11, FirstName = "Ольга", MiddleName = "Ивановна", LastName = "Комарова", DepartmentId = 4, AcademicRank = "старший преподаватель", Email = "o.i.komarova@mail.ru" },
                new TeachersData { Id = 12, FirstName = "Анатолий", MiddleName = "Васильевич", LastName = "Ореховский", DepartmentId = 5, AcademicRank = "доцент", SupervisorId = 2 }
            };
        }

        public TeachersGenerator(int num, int departments_num)
        {
            Generate(num, departments_num);
        }

        public class TeachersData
        {
            public int Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string MiddleName { get; set; }
            public string AcademicRank { get; set; }
            public string Email { get; set; }
            public int DepartmentId { get; set; }
            public int? SupervisorId { get; set; }

            public override string ToString()
            {
                return string.Join
                (
                    "\t",
                    Id.ToString(),
                    FirstName,
                    MiddleName,
                    LastName,
                    AcademicRank,
                    Email,
                    DepartmentId.ToString(),
                    SupervisorId.ToString()
                );
            }
        }

        private string[] _ranks = { "доцент", "профессор", "аспирант", "академик" };
        private string[] _male_fn = { "Александр", "Дмитрий", "Максим", "Даниил", "Кирилл", "Ярослав",
            "Денис", "Никита", "Иван", "Артем", "Тимофей", "Богдан", "Глеб", "Захар", "Матвей" };
        private string[] _female_fn = { "Анна", "Дарья", "Мария", "Марина", "Наталья", "Ирина" };
        private string[] _male_ln = { "Александров", "Дмитриев", "Максимов", "Даниилов", "Кириллов", "Славин",
            "Денисов", "Никитин", "Иванов", "Артемов", "Тимофеев", "Богданов", "Глебов", "Захаров", "Матвеев" };
        private string[] _female_ln = { "Александрова", "Дмитриева", "Максимова", "Даниилова", "Кириллова",
            "Славина", "Денисова", "Никитина", "Иванова", "Артемова", "Тимофеев", "Богданова", "Глебова",
            "Захарова", "Матвеева" };
        private string[] _male_mn = { "Александрович", "Дмитриевич", "Максимович", "Даниилович", "Кириллович",
            "Денисович", "Никитьевич", "Иванович", "Артемович", "Тимофеевич", "Богданович",
            "Глебович", "Захарович", "Матвеевич" };
        private string[] _female_mn = { "Александровна", "Дмитриевна", "Максимовна", "Данииловна", "Кирилловна",
            "Денисовна", "Никитьевна", "Ивановна", "Артемовна", "Тимофеевна", "Богдановна", "Глебовна",
            "Захаровна", "Матвеевна" };

        public List<TeachersData> Data { get; private set; }

        public void Generate(int num, int departments_num)
        {
            if (num <= 0)
                return;

            GenerateData(num, departments_num);
            GenerateSupervisors();
        }

        public void SaveUnicode(string path)
        {
            var encoding = Encoding.Unicode;
            var full_path = Path.Combine(path, "Teachers.txt");
            using (var stream = new StreamWriter(full_path, false, encoding))
            {
                for (int i = 0; i < Data.Count; ++i)
                    stream.WriteLine(Data[i].ToString());
            }
        }

        private void GenerateSupervisors()
        {
            var rand = new Random();
            for (int i = 0; i < Data.Count-1; ++i)
            {
                if (rand.Next(2) == 0)
                    for (int j = i + 1; j < Data.Count; ++j)
                        if ((Data[i].DepartmentId == Data[j].DepartmentId) && (rand.Next(2) == 0))
                            Data[j].SupervisorId = Data[i].Id;                            
            }
        }

        private void GenerateData(int num, int departments_num)
        {
            var rand = new Random();
            Data = new List<TeachersData>();
            for (int i = 0; i < num; ++i)
            {
                var item = new TeachersData();
                item.Id = i + 1;

                if (rand.Next(2) == 0)
                {
                    item.FirstName = _male_fn[rand.Next(_male_fn.Count())];
                    item.MiddleName = _male_mn[rand.Next(_male_mn.Count())];
                    item.LastName = _male_ln[rand.Next(_male_ln.Count())];
                }
                else
                {
                    item.FirstName = _female_fn[rand.Next(_female_fn.Count())];
                    item.MiddleName = _female_mn[rand.Next(_female_mn.Count())];
                    item.LastName = _female_ln[rand.Next(_female_ln.Count())];
                }

                item.DepartmentId = rand.Next(1, departments_num + 1);
                item.SupervisorId = null;

                Data.Add(item);
            }
        }

    }
}

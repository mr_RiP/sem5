﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFiller
{
    class Program
    {
        static void Main(string[] args)
        {
            int user = -1;
            while (user != 0)
            {
                user = Menu();
                switch (user)
                {
                    case 1:
                        ResetRandom();
                        user = 0;
                        break;
                    case 2:
                        ResetActual();
                        user = 0;
                        break;
                    default:
                        ErrorMsg();
                        break;
                }
            }
        }

        private static void ResetActual()
        {
            string path = @"D:\Documents\sem5\Databases\Lab1\TestData";

            new DepartmentsGenerator().SaveUnicode(path);
            new TeachersGenerator().SaveUnicode(path);
            new CoursesGenerator().SaveUnicode(path);
            new TimeFramesGenerator().SaveUnicode(path);
            new ClassesGenerator().SaveUnicode(path);
        }

        private static void ResetRandom()
        {
            int departments_num = 200;
            int teachers_num = 2000;
            int courses_num = 1000;
            string path = @"D:\Documents\sem5\Databases\Lab1\TestData";

            new DepartmentsGenerator(departments_num).SaveUnicode(path);
            new TeachersGenerator(teachers_num, departments_num).SaveUnicode(path);
            new CoursesGenerator(courses_num).SaveUnicode(path);
            new TimeFramesGenerator().SaveUnicode(path);
            new ClassesGenerator().SaveUnicode(path);
        }

        private static void ErrorMsg()
        {
            throw new NotImplementedException();
        }

        private static int Menu()
        {
            Console.WriteLine("1. Reset random data");
            Console.WriteLine("2. Reset actual data");
            Console.WriteLine("0. Exit");
            return (int)Char.GetNumericValue(Console.ReadKey().KeyChar);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFiller
{
    class DepartmentsGenerator
    {

        public DepartmentsGenerator()
        {
            GenerateActual();
        }

        public DepartmentsGenerator(int num)
        {
            Generate(num);
        }

        public class DepartmentsData
        {
            public int Id { get; set; }
            public string Faculty { get; set; }
            public int? Index { get; set; }
            public string Description { get; set; }

            public override string ToString()
            {
                return Id.ToString() + "\t" + Faculty + "\t" + Index.ToString() + "\t" + Description;
            }
        }

        public List<DepartmentsData> Data { get; private set; }      
        const string _alphabet = "АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЭЮЯ";

        public void Generate(int num)
        {
            if (num <= 0)
                return;

            Data = new List<DepartmentsData>();
            Random rand = new Random();

            for (int i = 0; i < num; ++i)
            {
                var row = new DepartmentsData();
                row.Id = i + 1;
                row.Faculty = GenerateFaculty(rand);
                row.Index = 0;

                Data.Add(row);
            }

            GenerateIndices();
        }

        public void SaveUnicode(string path)
        {
            var encoding = Encoding.Unicode;
            var full_path = Path.Combine(path, "Departments.txt");
            using (var stream = new StreamWriter(full_path, false, encoding))
            {
                for (int i = 0; i < Data.Count; ++i)
                    stream.WriteLine(Data[i].ToString());
            }
        }

        private void GenerateIndices()
        {
            for (int i = 0; i < Data.Count; ++i)
                if (Data[i].Index == 0)
                {
                    int n = 1;
                    for (int j = i; j < Data.Count; ++j)
                        if (Data[j].Faculty == Data[i].Faculty)
                            Data[j].Index = n++;
                }
        }

        private string GenerateFaculty(Random rand)
        {
            int n = rand.Next(1, 4);
            char[] literals = new char[n];
            for (int i=0; i<n; ++i)
                literals[i] = _alphabet[rand.Next(_alphabet.Length)];
            return new string(literals);
        }

        public void GenerateActual()
        {
            Data = new List<DepartmentsData>
            {
                new DepartmentsData { Id = 1, Faculty = "ИУ", Index = 7 },
                new DepartmentsData { Id = 2, Faculty = "ИУ", Index = 6 },
                new DepartmentsData { Id = 3, Faculty = "ФН", Index = 12 },
                new DepartmentsData { Id = 4, Faculty = "Л",  Index = 2 },
                new DepartmentsData { Id = 5, Faculty = "СГН", Index = 3 },
                new DepartmentsData { Id = 6, Faculty = "ФВ" }
            };
        }
    }
}

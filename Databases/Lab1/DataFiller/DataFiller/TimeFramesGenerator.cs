﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DataFiller
{
    internal class TimeFramesGenerator
    {
        public TimeFramesGenerator()
        {
            Generate();
        }

        public class TimeFramesData
        {
            public int Id { get; set; }
            public int Day { get; set; }
            public string Begin { get; set; }
            public string End { get; set; }
            public bool Nominator { get; set; }
            public bool Denominator { get; set; }
            public string Description { get; set; }

            public override string ToString()
            {
                return string.Join
                (
                    "\t",
                    Id.ToString(),
                    Day.ToString(),
                    Begin,
                    End,
                    BoolString(Nominator),
                    BoolString(Denominator),
                    Description
                );
            }

            private static string BoolString(bool bit)
            {
                return (bit) ? "1" : "0";
            }
        }

        public List<TimeFramesData> Data { get; private set; }

        private string[] _sport_time_begin = { "08:15", "10:00", "12:20", "14:05", "15:50", "17:30" };
        private string[] _sport_time_end = { "09:45", "11:30", "13:50", "15:35", "17:20", "19:00" };
        private string[] _general_time_begin = { "08:30", "10:15", "12:00", "13:50", "15:40", "17:25", "19:10" };
        private string[] _general_time_end = { "10:05", "11:50", "13:35", "15:25", "17:15", "19:00", "20:45" };

        public void Generate()
        {
            Data = new List<TimeFramesData>();
            int id = 0;

            for (int i = 0; i < _general_time_begin.Length; ++i)
                for (int j = 1; j < 7; ++j)
                {
                    var item = new TimeFramesData();
                    item.Id = ++id;
                    item.Day = j;
                    item.Begin = _general_time_begin[i];
                    item.End = _general_time_end[i];
                    item.Nominator = true;
                    item.Denominator = false;
                    item.Description = "Основное расписание (ЧС)";
                    Data.Add(item);

                    item = new TimeFramesData();
                    item.Id = ++id;
                    item.Day = j;
                    item.Begin = _general_time_begin[i];
                    item.End = _general_time_end[i];
                    item.Nominator = false;
                    item.Denominator = true;
                    item.Description = "Основное расписание (ЗН)";
                    Data.Add(item);

                    item = new TimeFramesData();
                    item.Id = ++id;
                    item.Day = j;
                    item.Begin = _general_time_begin[i];
                    item.End = _general_time_end[i];
                    item.Nominator = true;
                    item.Denominator = true;
                    item.Description = "Основное расписание (ЧС+ЗН)";
                    Data.Add(item);
                }

            for (int i = 0; i < _sport_time_begin.Count(); ++i)
                for (int j = 1; j < 8; ++j)
                {
                    var item = new TimeFramesData();
                    item.Id = ++id;
                    item.Day = j;
                    item.Begin = _sport_time_begin[i];
                    item.End = _sport_time_end[i];
                    item.Nominator = true;
                    item.Denominator = false;
                    item.Description = "Расписание спорткомплекса (ЧС)";
                    Data.Add(item);

                    item = new TimeFramesData();
                    item.Id = ++id;
                    item.Day = j;
                    item.Begin = _sport_time_begin[i];
                    item.End = _sport_time_end[i];
                    item.Nominator = false;
                    item.Denominator = true;
                    item.Description = "Расписание спорткомплекса (ЗН)";
                    Data.Add(item);

                    item = new TimeFramesData();
                    item.Id = ++id;
                    item.Day = j;
                    item.Begin = _sport_time_begin[i];
                    item.End = _sport_time_end[i];
                    item.Nominator = true;
                    item.Denominator = true;
                    item.Description = "Расписание спорткомплекса (ЧС+ЗН)";
                    Data.Add(item);
                }

        }

        public void SaveUnicode(string path)
        {
            var encoding = Encoding.Unicode;
            var full_path = Path.Combine(path, "TimeFrames.txt");
            using (var stream = new StreamWriter(full_path, false, encoding))
            {
                for (int i = 0; i < Data.Count; ++i)
                    stream.WriteLine(Data[i].ToString());
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFiller
{
    class CoursesGenerator
    {
        public CoursesGenerator()
        {
            GenerateActual();
        }

        public CoursesGenerator(int num)
        {
            Generate(num);
        }

        public class CoursesData
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }

            public override string ToString()
            {
                return Id.ToString() + '\t' + Name + '\t' + Description;
            }
        }

        public List<CoursesData> Data { get; private set; }

        private string[] _name = { "Алгебра", "Геометрия", "Физика", "Химия", "Экономика", "География", "Информатика", "Философия", "Политология", "Психология", "Социология",
            "История", "Математика", "Оптика", "Биология" };

        private string[] _prefix = { "Аналитическая", "Теоретическая", "Фундаметальная", "Практическая", "Информационная", "Классическая" };

        private string[] _postfix = {"в алгебре", "в геометрии", "в физике", "в химии", "в экономике", "в географии", "в информатике", "в философии", "в политике", "в психологии",
            "в социологии", "в истории", "в математике", "в оптике", "в биологии", "в мировой политике", "в современном обществе" };

        public void Generate(int num)
        {
            var rand = new Random();
            Data = new List<CoursesData>();
            for (int i = 0; i < num; ++i)
            {
                int name_index = rand.Next(_name.Count());
                int prefix_index = rand.Next(-1, _prefix.Count());
                int postfix_index = rand.Next(-1, _postfix.Count());
                while (postfix_index == name_index)
                    postfix_index = rand.Next(-1, _postfix.Count());

                var item = new CoursesData();
                item.Id = i + 1;
                item.Name = _name[name_index];
                if (prefix_index != -1)
                    item.Name = _prefix[prefix_index] + ' ' + item.Name;
                if (postfix_index != -1)
                    item.Name = item.Name + ' ' + _postfix[postfix_index];

                Data.Add(item);
            }

        }

        public void SaveUnicode(string path)
        {
            var encoding = Encoding.Unicode;
            var full_path = Path.Combine(path, "Courses.txt");
            using (var stream = new StreamWriter(full_path, false, encoding))
            {
                for (int i = 0; i < Data.Count; ++i)
                    stream.WriteLine(Data[i].ToString());
            }
        }

        public void GenerateActual()
        {
            Data = new List<CoursesData>
            {
                new CoursesData { Id = 1, Name = "Базы данных" },
                new CoursesData { Id = 2, Name = "Анализ алгоритмов" },
                new CoursesData { Id = 3, Name = "Политология" },
                new CoursesData { Id = 4, Name = "Операционные системы" },
                new CoursesData { Id = 5, Name = "Архитектура ЭВМ" },
                new CoursesData { Id = 6, Name = "Физическое воспитание" },
                new CoursesData { Id = 7, Name = "Иностранный язык" },
                new CoursesData { Id = 8, Name = "Теория вероятностей" }
            };
        }
    }
}

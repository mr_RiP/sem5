﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFiller
{
    class ClassesGenerator
    {
        public ClassesGenerator()
        {
            GenerateActual();
        }

        public void GenerateActual()
        {
            var lab = "лабораторная работа";
            var sem = "семинар";
            var lect = "лекция";

            Data = new List<ClassesData>
            {
                new ClassesData { Id = 1, TimeId = 75, CourseId = 1, TeacherId = 3, ClassType = lect, ClassRoom = "228л" },
                new ClassesData { Id = 2, TimeId = 93, CourseId = 3, TeacherId = 8, ClassType = sem, ClassRoom = "615л" },
                new ClassesData { Id = 3, TimeId = 109, CourseId = 2, TeacherId = 5, ClassType = lab, ClassRoom = "829л" },
                new ClassesData { Id = 4, TimeId = 110, CourseId = 2, TeacherId = 5, ClassType = sem, ClassRoom = "829л" },

                new ClassesData { Id = 5, TimeId = 45, CourseId = 5, TeacherId = 10, ClassType = lab, ClassRoom = "805" },
                new ClassesData { Id = 6, TimeId = 63, CourseId = 4, TeacherId = 6, ClassType = lab, ClassRoom = "508л" },
                new ClassesData { Id = 7, TimeId = 79, CourseId = 1, TeacherId = 3, ClassType = lect, ClassRoom = "1108л" },
                new ClassesData { Id = 8, TimeId = 80, CourseId = 4, TeacherId = 6, ClassType = lect, ClassRoom = "1108л" },
                new ClassesData { Id = 9, TimeId = 99, CourseId = 4, TeacherId = 6, ClassType = lect, ClassRoom = "1108л" },
                new ClassesData { Id = 10, TimeId = 117, CourseId = 8, TeacherId = 7, ClassType = sem, ClassRoom = "1015л" },

                new ClassesData { Id = 11, TimeId = 66, CourseId = 8, TeacherId = 7, ClassType = lect, ClassRoom = "228л" },
                new ClassesData { Id = 12, TimeId = 84, CourseId = 7, TeacherId = 11, ClassType = sem, ClassRoom = "407л" },
                new ClassesData { Id = 13, TimeId = 101, CourseId = 3, TeacherId = 12, ClassType = lect, ClassRoom = "218л" },

                new ClassesData { Id = 14, TimeId = 49, CourseId = 1, TeacherId = 3, ClassType = lab, ClassRoom = "511л" },
                new ClassesData { Id = 15, TimeId = 67, CourseId = 1, TeacherId = 3, ClassType = lab, ClassRoom = "511л" },
                new ClassesData { Id = 16, TimeId = 87, CourseId = 5, TeacherId = 10, ClassType = lect, ClassRoom = "228л" },
                new ClassesData { Id = 17, TimeId = 103, CourseId = 2, TeacherId = 4, ClassType = lect, ClassRoom = "228л" },
                new ClassesData { Id = 18, TimeId = 104, CourseId = 2, TeacherId = 5, ClassType = sem, ClassRoom = "228л" },
                new ClassesData { Id = 19, TimeId = 121, CourseId = 2, TeacherId = 4, ClassType = lect, ClassRoom = "228л" }
            };

            Data.ForEach(item =>
            {
                item.BeginDate = "2016-09-01";
                item.EndDate = "2016-12-24";
            });
        }

        public class ClassesData
        {
            public int Id { get; set; }
            public int TeacherId { get; set; }
            public int TimeId { get; set; }
            public int CourseId { get; set; }
            public string ClassType { get; set; }
            public string ClassRoom { get; set; }
            public string BeginDate { get; set; }
            public string EndDate { get; set; }

            public override string ToString()
            {
                return string.Join
                (
                    "\t",
                    Id.ToString(),
                    TeacherId.ToString(),
                    TimeId.ToString(),
                    CourseId.ToString(),
                    ClassType,
                    ClassRoom,
                    BeginDate,
                    EndDate
                );
            }
        }

        public List<ClassesData> Data { get; private set; }

        public void SaveUnicode(string path)
        {
            var encoding = Encoding.Unicode;
            var full_path = Path.Combine(path, "Classes.txt");
            using (var stream = new StreamWriter(full_path, false, encoding))
            {
                for (int i = 0; i < Data.Count; ++i)
                    stream.WriteLine(Data[i].ToString());
            }
        }
    }
}

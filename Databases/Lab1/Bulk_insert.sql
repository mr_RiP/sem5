USE [Timetable]
GO

BULK INSERT [Timetable].[dbo].[Departments]
FROM 'D:\Documents\sem5\Databases\Lab1\TestData\Departments.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[Teachers]
FROM 'D:\Documents\sem5\Databases\Lab1\TestData\Teachers.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[Courses]
FROM 'D:\Documents\sem5\Databases\Lab1\TestData\Courses.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[TimeFrames]
FROM 'D:\Documents\sem5\Databases\Lab1\TestData\TimeFrames.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[Classes]
FROM 'D:\Documents\sem5\Databases\Lab1\TestData\Classes.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO
USE [master]
GO

IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'Timetable')
	DROP DATABASE [Timetable]
GO

CREATE DATABASE [Timetable]
GO

USE [Timetable]
GO

CREATE TABLE [dbo].[Courses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](512) NULL
)
GO

CREATE TABLE [dbo].[TimeFrames](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Day] [int] NOT NULL,
	[Begin] [time](0) NOT NULL,
	[End] [time](0) NOT NULL,
	[Nominator] [bit] NOT NULL,
	[Denominator] [bit] NOT NULL,
	[Description] [nvarchar](128) NULL
)
GO

CREATE TABLE [dbo].[Departments]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Faculty] [nvarchar](8) NOT NULL,
	[Index] [int] NULL,
	[Description] [nvarchar](512) NULL
)
GO

CREATE TABLE [dbo].[Teachers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](16) NOT NULL,
	[MiddleName] [nvarchar](16) NOT NULL,
	[LastName] [nvarchar](16) NOT NULL,
	[AcademicRank] [nvarchar](32) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[DepartmentId] [int] NOT NULL,
	[SupervisorId] [int] NULL
)
GO

CREATE TABLE [dbo].[Classes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeacherId] [int] NULL,
	[TimeId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[Type] [nvarchar](32) NOT NULL,
	[Room] [nvarchar](16) NOT NULL,
	[BeginDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL
)
GO
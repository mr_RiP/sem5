USE [Timetable]
GO

-- 21.	Инструкция DELETE с вложенным коррелированным подзапросом в предложении WHERE.
-- Удалим все занятия по политологии, анализу алгоритмов и операционным системам
DELETE [Classes]
WHERE [CourseId] IN
(
	SELECT [Id]
	FROM [Courses]
	WHERE [Name] = N'Политология' OR
		[Name] = N'Операционные системы' OR
		[Name] = N'Анализ алгоритмов'
)
GO

-- 22.	Инструкция SELECT, использующая простое обобщенное табличное выражение.
-- Выведем детальную информацию о занятиях в понедельник - числитель

WITH [Data] AS
(
	SELECT 
		[Classes].[Id] AS [Id],
		[TimeFrames].[Day] AS [Day],
		[TimeFrames].[Nominator] AS [Nominator],
		[TimeFrames].[Denominator] AS [Denominator],
		[TimeFrames].[Begin] AS [Time],
		[Courses].[Name] AS [Course],
		[Classes].[Type] AS [Type],
		[Classes].[Room] AS [Room],
		CONCAT([Teachers].[LastName], ' ', [Teachers].[FirstName], ' ', [Teachers].[MiddleName]) AS [Teacher]
	FROM [Classes] INNER JOIN [Courses] ON [Classes].[CourseId] = [Courses].[Id]
		INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
		INNER JOIN [Teachers] ON [Classes].[TeacherId] = [Teachers].[Id]
)
SELECT
	[Id],
	[Time],
	[Course],
	[Type],
	[Room],
	[Teacher]
FROM [Data]
WHERE [Day] = 1 AND [Nominator] = 1
GO

-- 23.	Инструкция SELECT, использующая рекурсивное обобщенное табличное выражение.
-- Вывести древо руководства для кафедры ИУ-7
WITH [CommandTree] AS
(
	SELECT
		[Id],
		[FirstName],
		[MiddleName],
		[LastName],
		[AcademicRank],
		1 AS [InnerRank]
	FROM [Teachers]
	WHERE [DepartmentId] = 1 AND [SupervisorId] IS NULL

	UNION ALL
	SELECT 
		[Teachers].[Id] AS [Id],
		[Teachers].[FirstName] AS [FirstName],
		[Teachers].[MiddleName] AS [MiddleName],
		[Teachers].[LastName] AS [LastName],
		[Teachers].[AcademicRank] AS [AcademinRank],
		[CommandTree].[InnerRank] + 1 AS [InnerRank]
	FROM [Teachers] INNER JOIN [CommandTree] ON [Teachers].[SupervisorId] = [CommandTree].[Id]
)
SELECT *
FROM [CommandTree]
GO

-- 24.	Использование операторов PIVOT и UNPIVOT в предложении FROM инструкции SELECT
-- Для каждого преподавателя вывести сколько занятий какого типа он ведет в течение 2 недель

WITH [TwoWeekClasses] AS
(
	SELECT
		[Classes].[Id] AS [ClassId],
		[Classes].[Type] AS [ClassType],
		[Classes].[TeacherId] AS [TeacherId]
	FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
	WHERE [TimeFrames].[Nominator] = 1

	UNION ALL

	SELECT
		[Classes].[Id] AS [ClassId],
		[Classes].[Type] AS [ClassType],
		[Classes].[TeacherId] AS [TeacherId]
	FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
	WHERE [TimeFrames].[Denominator] = 1
),
[TeacherClasses] AS 
(
	SELECT 
		[TeacherId],
		CASE [ClassType]
			WHEN N'лабораторная работа' THEN 'Labs'
			WHEN N'лекция' THEN 'Lectures'
			WHEN N'семинар' THEN 'Seminars'
		END AS [ClassType],
		COUNT([ClassType]) AS [Count]
	FROM [TwoWeekClasses]
	GROUP BY [TeacherId], [ClassType]
)
SELECT
	[Id],
	CONCAT([LastName],' ',[FirstName],' ',[MiddleName]) AS [Name],
	ISNULL([Labs], 0) AS [Labs],
	ISNULL([Lectures], 0) AS [Lectures],
	ISNULL([Seminars], 0) AS [Seminars]
INTO [#TeacherStats]
FROM 
( 
	SELECT * 
	FROM [Teachers] LEFT OUTER JOIN [TeacherClasses] ON [Teachers].[Id] = [TeacherClasses].[TeacherId]
) src
PIVOT
(
	MAX([Count])
	FOR [ClassType]
	IN ([Labs], [Lectures], [Seminars])
) pvt
GO

SELECT *
FROM [#TeacherStats]
GO

SELECT 
	[Id],
	[Name],
	[Type],
	[Count]
FROM [#TeacherStats] ts
UNPIVOT
(
	[Count] FOR [Type] IN ([Labs], [Lectures], [Seminars])
) upvt
GO

DROP TABLE [#TeacherStats]
GO
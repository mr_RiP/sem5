USE [Timetable]
GO

-- 1.	���������� SELECT, ������������ �������� ���������.
-- ������� �������, ���������� �� ������ � ���������

WITH [ClassesWithCourseNames] AS
(
	SELECT
		[Classes].[Id] AS [Id],
		[TimeId],
		[Name] AS [CourseName],
		[Type],
		[Room]
	FROM [Classes] INNER JOIN [Courses]  ON [Classes].[CourseId] = [Courses].[Id]
)
SELECT
	[ClassesWithCourseNames].[Id] AS [Id],
	[CourseName],
	[Type],
	[Room],
	[Begin] AS [Time]
FROM [ClassesWithCourseNames] INNER JOIN [TimeFrames] ON [ClassesWithCourseNames].[TimeId] = [TimeFrames].[Id]
WHERE [Day] = 3 AND [Nominator] = 1
GO
	
-- 2.	���������� SELECT, ������������ �������� BETWEEN.
-- ����� ������� ���������� � 13.00 �� 16.00 ������������
SELECT DISTINCT
	[Name],
	[Type]
FROM [Classes] INNER JOIN [Courses] ON [Classes].[CourseId] = [Courses].[Id] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
WHERE [TimeFrames].[Begin] BETWEEN TIMEFROMPARTS(13,0,0,0,0) AND TIMEFROMPARTS(16,0,0,0,0)
GO

-- 3.	���������� SELECT, ������������ �������� LIKE.
-- ��� �������������, ������� ������� ������������ �� "��"
SELECT
	[Teachers].[Id] AS [Id],
	[LastName],
	[FirstName],
	[MiddleName],
	CONCAT([Faculty],N'-',[Index]) AS [Department]
FROM [Teachers] INNER JOIN [Departments] ON [Teachers].[DepartmentId] = [Departments].[Id]
WHERE [LastName] LIKE N'%��'
GO

-- 4.	���������� SELECT, ������������ �������� IN � ��������� �����������.
-- �������� ���� �������������� � ���������� ��
SELECT
	[Teachers].[Id] AS [Id],
	[LastName],
	[FirstName],
	[MiddleName],
	CONCAT([Faculty],N'-',[Index]) AS [Department]
FROM [Teachers] INNER JOIN [Departments] ON [Teachers].[DepartmentId] = [Departments].[Id]
WHERE [Teachers].[DepartmentId] IN 
(
	SELECT [Id]
	FROM [Departments]
	WHERE [Faculty] = N'��'
)
GO

-- 5.	���������� SELECT, ������������ �������� EXISTS � ��������� �����������.
-- ��� �����, �� ������� ������������� ������������ ������
SELECT DISTINCT
	[Id],
	[Name]
FROM [Courses]
WHERE EXISTS
(
	SELECT DISTINCT [CourseId]
	FROM [Classes]
	WHERE [Type] = N'������������ ������' AND [Classes].[CourseId] = [Courses].[Id]
)
GO

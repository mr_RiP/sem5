USE [Timetable]
GO

-- 6.	���������� SELECT, ������������ �������� ��������� � ���������.
-- ����� ������� ������ ����� �����
SELECT
	[Courses].[Name] AS [Name],
	[Classes].[Type] AS [Type],
	[TimeFrames].[Nominator] AS [Nominator],
	[TimeFrames].[Denominator] AS [Denominator],
	[TimeFrames].[Day] AS [Day],
	[TimeFrames].[Begin] AS [Time]
FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id] INNER JOIN [Courses] ON [Classes].[CourseId] = [Courses].[Id]
WHERE [TimeFrames].[Begin] >= ALL
(
	SELECT DISTINCT [TimeFrames].[Begin] AS [Begin]
	FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
)
GO

-- 7.	���������� SELECT, ������������ ���������� ������� � ���������� ��������.
-- ������� ������� � ������� ���������� ��� ������ ������� ����������
WITH 
[NominatorClasses] AS
(
	SELECT
		[CourseId],
		COUNT([CourseId]) AS [ClassTimes]
	FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
	WHERE [TimeFrames].[Nominator] = 1
	GROUP BY [CourseId]
),
[DenominatorClasses] AS
(
	SELECT
		[CourseId],
		COUNT([CourseId]) AS [ClassTimes]
	FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
	WHERE [TimeFrames].[Denominator] = 1
	GROUP BY [CourseId]
)
SELECT
	[Courses].[Id] AS [Id],
	[Courses].[Name] AS [Name],
	ISNULL([NominatorClasses].[ClassTimes], 0) AS [NominatorTimes],
	ISNULL([DenominatorClasses].[ClassTimes], 0) AS [DenominatorTimes],
	ISNULL(([NominatorClasses].[ClassTimes] + [DenominatorClasses].[ClassTimes]) / 2.0, 0) AS [AverageWeeklyTimes]
FROM [Courses] LEFT OUTER JOIN [NominatorClasses] ON [Courses].[Id] = [NominatorClasses].[CourseId] 
	LEFT OUTER JOIN [DenominatorClasses] ON [Courses].[Id] = [DenominatorClasses].[CourseId]
ORDER BY [AverageWeeklyTimes] DESC
GO

-- 8.	���������� SELECT, ������������ ��������� ���������� � ���������� ��������.
-- ������� ��� ������ ���������� ����� ������ ������ ������� ������� � ������ ��������
WITH [ClassTime] AS
(
	SELECT
		[Classes].[Id] AS [ClassId],
		[Classes].[CourseId] AS [CourseId],
		[TimeFrames].[Begin] AS [Time]
	FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
)
SELECT 
	[Courses].[Id] AS [CourseId],
	[Courses].[Name] AS [Name],
	(
		SELECT MIN([Time])
		FROM [ClassTime]
		WHERE [ClassTime].[CourseId] = [Courses].[Id]
	) AS [MinTime],
	(
		SELECT MAX([Time])
		FROM [ClassTime]
		WHERE [ClassTime].[CourseId] = [Courses].[Id]
	) AS [MaxTime]
FROM [Courses]

-- 9.	���������� SELECT, ������������ ������� ��������� CASE.
-- ������� ���������� ������� ��� ������-��������� �� ��������� ������������ ��� ������
SELECT 
	CASE [TimeFrames].[Day]
		WHEN 1 THEN N'�����������'
		WHEN 2 THEN N'�������'
		WHEN 3 THEN N'�����'
		WHEN 4 THEN N'�������'
		WHEN 5 THEN N'�������'
		WHEN 6 THEN N'�������'
		WHEN 7 THEN N'�����������'
	END AS [Day],
	[Classes].[Id] AS [Id],
	[Courses].[Name] AS [Name],
	[Classes].[Type] AS [Type],
	[TimeFrames].[Begin] AS [Time]
FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
	INNER JOIN [Courses] ON [Classes].[CourseId] = [Courses].[Id]
WHERE [TimeFrames].[Nominator] = 1
GO

-- 10.	���������� SELECT, ������������ ��������� ��������� CASE.
-- ��� ������� ��� �� ������� ������ ��������� ���������� ��� ������������� ���������
WITH [DayClasses] AS
(
	SELECT DISTINCT
		[TimeFrames].[Day] AS [Day],
		COUNT([TimeFrames].[Day]) AS [ClassesCount]
	FROM [TimeFrames] INNER JOIN [Classes] ON [TimeFrames].[Id] = [Classes].[TimeId]
	WHERE [TimeFrames].[Nominator] = 1
	GROUP BY [TimeFrames].[Day]
)
SELECT DISTINCT
	[TimeFrames].[Day] AS [Day],
	CASE
		WHEN ISNULL([DayClasses].[ClassesCount], 0) = 0  THEN N'������� ���'
		WHEN ISNULL([DayClasses].[ClassesCount], 0) <= 2 THEN N'������'
		WHEN ISNULL([DayClasses].[ClassesCount], 0) = 3  THEN N'����������'
		WHEN ISNULL([DayClasses].[ClassesCount], 0) >= 4 THEN N'�������'
	END AS [Difficulty]
FROM [TimeFrames] LEFT OUTER JOIN [DayClasses] ON [TimeFrames].[Day] = [DayClasses].[Day]
GO
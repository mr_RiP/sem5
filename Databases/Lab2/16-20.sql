USE [Timetable]
GO

-- 16.	������������ ���������� INSERT, ����������� ������� � ������� ����� ������ ��������.
-- ������� � ������ �������������� ���������� ����� �������������
INSERT [Teachers] 
(
	[FirstName], 
	[MiddleName], 
	[LastName], 
	[AcademicRank], 
	[DepartmentId],
	[SupervisorId]
)
VALUES
(
	N'�����',
	N'������������',
	N'���������',
	N'������',
	1,
	1
)
GO

-- 17.	������������� ���������� INSERT, ����������� ������� � ������� ��������������� ������ ������ ���������� ����������.
-- ������� ��� ���� ������� �� ������ ������������ � ������� � 12.00 � ��������� 1031�, ������� � ������������ ���
INSERT [Classes]
(
	[TeacherId],
	[TimeId],
	[CourseId], 
	[Type], 
	[Room], 
	[BeginDate], 
	[EndDate]
) 
SELECT
	[TeacherId],
	(
		SELECT TOP 1 [Id]
		FROM [TimeFrames]
		WHERE [Nominator] = 1 AND [Denominator] = 1 AND [Day] = 4 AND [Begin] = TIMEFROMPARTS(12,0,0,0,0)
	) AS [TimeId],
	[CourseId],
	[Type],
	N'1031�' AS [Room],
	GETDATE() AS [BeginDate],
	[EndDate]
FROM [Classes]
WHERE [Type] = N'�������' AND [CourseId] =
(
	SELECT TOP 1 [Id]
	FROM [Courses]
	WHERE [Name] = N'������ ������������'
)
GO

-- 18.	������� ���������� UPDATE.
-- ������� ������� �������� �������� �� 31 �������
UPDATE [Classes]
SET [EndDate] = DATEFROMPARTS(YEAR([EndDate]), 12, 31)
WHERE GETDATE() BETWEEN [BeginDate] AND [EndDate]
GO

-- 19.	���������� UPDATE �� ��������� ����������� � ����������� SET.
-- 

-- 20.	������� ���������� DELETE.
-- ������ �������, ������� ������ ���� ���������� �������
DELETE [Classes]
WHERE [BeginDate] = CAST(GETDATE() AS date)
GO

USE [Timetable]
GO

-- 11.	�������� ����� ��������� ��������� ������� �� ��������������� ������ ������ ���������� SELECT.
-- ������� ���� �������������� ������� ��7 
SELECT *
INTO [#IU7]
FROM [Teachers]
WHERE [Teachers].[DepartmentId] IN
(
	SELECT [Id]
	FROM [Departments]
	WHERE [Faculty] = N'��' AND [Index] = 7
)
GO

SELECT *
FROM [#IU7]
GO

DROP TABLE [#IU7]
GO

-- 12.	���������� SELECT, ������������ ��������� ��������������� ���������� � �������� ����������� ������ � ����������� FROM.
-- ������� 3 ��������������, �������� ����������� ���������
SELECT *
FROM [Teachers] INNER JOIN
(
	SELECT TOP 3 
		[Teachers].[Id] AS [Id],
		COUNT([Teachers].[Id]) AS [Count]
	FROM [Teachers] INNER JOIN
	(	
		(
			SELECT 
				[Classes].[Id] AS [ClassId],
				[Classes].[TeacherId] AS [TeacherId]
			FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
			WHERE [TimeFrames].[Nominator] = 1
		)
		UNION ALL
		(
			SELECT 
				[Classes].[Id] AS [ClassId],
				[Classes].[TeacherId] AS [TeacherId]
			FROM [Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id]
			WHERE [TimeFrames].[Denominator] = 1
		)
	) AS [DistinctWeekClasses] ON [Teachers].[Id] = [DistinctWeekClasses].[TeacherId]
	GROUP BY [Teachers].[Id]
	ORDER BY [Count] DESC

) AS [MostWorkingTeachers]
ON [Teachers].[Id] = [MostWorkingTeachers].[Id]
GO

-- 13.	���������� SELECT, ������������ ��������� ���������� � ������� ����������� 3.
-- 
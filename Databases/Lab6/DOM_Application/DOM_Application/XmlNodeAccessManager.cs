﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DOM_Application
{
    class XmlNodeAccessManager : ConsoleMenu
    {
        protected XmlNodeAccessManager() { }

        public XmlNodeAccessManager(XmlFileManager file)
        {
            _file = file;
        }

        private XmlFileManager _file;

        protected override bool ExecuteCommand(char cmd)
        {
            switch (cmd)
            {
                case '0':
                    return true;
                case '1':
                    XmlChildNodeAccess();
                    return false;
                default:
                    return IncorrectCommandOutput();
            }
        }

        private void XmlChildNodeAccess()
        {
            int num = 0;
            if (Int32.TryParse(GetStringData("Введите номер дочернего узла документа."), out num))
            {
                XmlNode node = _file.Document.DocumentElement.ChildNodes[num];
                if (node != null)
                {
                    switch (node.NodeType)
                    {
                        case XmlNodeType.Comment:
                            PrintXmlCommentData((XmlComment)node);
                            break;
                        case XmlNodeType.Element:
                            PrintXmlElementData((XmlElement)node);
                            break;
                        case XmlNodeType.Text:
                            PrintXmlTextData((XmlText)node);
                            break;
                        case XmlNodeType.ProcessingInstruction:
                            PrintXmlProcessingInstructionData((XmlProcessingInstruction)node);
                            break;
                        default:
                            Console.WriteLine("Узел не относится к типам XmlElement, XmlText, XmlComment или XmlProcessingInstruction.");
                            break;
                    }
                }
                else
                    Console.WriteLine("В документе нет {0} дочернего узла:", num);
            }
            else
                Console.WriteLine("Ошибка: Вводимое значение должно быть целым числом.");

            Pause();
        }

        private void PrintXmlProcessingInstructionData(XmlProcessingInstruction node)
        {
            Console.WriteLine("Тип узла: XmlProcessingInstruction");
            Console.WriteLine("Имя: " + node.Name);
            Console.WriteLine("Данные: " + node.Data);
        }

        private void PrintXmlTextData(XmlText node)
        {
            Console.WriteLine("Тип узла: XmlText");
            Console.WriteLine("Имя: " + node.Name);
            Console.WriteLine("Значение: " + node.Value);
        }

        private void PrintXmlElementData(XmlElement node)
        {
            Console.WriteLine("Тип узла: XmlElement");
            Console.WriteLine("Имя: " + node.Name);
            if (node.Value != null)
                Console.WriteLine("Значение: " + node.Value);
            if (node.HasAttributes)
            {
                Console.WriteLine("Аттрибуты:");
                foreach (XmlAttribute attr in node.Attributes)
                    Console.WriteLine("    {0} : {1}", attr.Name, attr.Value);
            }
            if (node.HasChildNodes)
            {
                Console.WriteLine("Дочерние узлы:");
                foreach (XmlNode child in node.ChildNodes)
                    Console.WriteLine("    {0} : {1}", child.Name, child.InnerText);
            }
        }

        private void PrintXmlCommentData(XmlComment node)
        {
            Console.WriteLine("Тип узла: XmlComment");
            Console.WriteLine("Имя: " + node.Name);
            Console.WriteLine("Значение: " + node.Value);
        }

        private void PrintChildData(XmlNode elem, int depth)
        {
            string nameStr = "\t" + Whitespaces(4 * depth) + elem.Name;
            if (elem.Value != null)
                Console.WriteLine(nameStr + ": " + elem.Value);
            else
                Console.WriteLine(nameStr);

            if (elem.HasChildNodes)
                foreach (XmlNode child in elem.ChildNodes)
                    PrintChildData(child, depth + 1);
        }

        private string Whitespaces(int num)
        {
            string str = "";
            for (int i = 0; i < num; i++)
                str = str + " ";
            return str;
        }

        protected override void WriteCommandList()
        {
            Console.WriteLine("== Доступ к содержимому узлов ==");
            Console.WriteLine("[1] к узлам типа XmlElement, XmlText, XmlComment, XmlProcessingInstruction");
            Console.WriteLine("[0] Назад");
        }
    }
}

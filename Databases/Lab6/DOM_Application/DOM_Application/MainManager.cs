﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOM_Application
{
    class MainManager : ConsoleMenu
    {
        public MainManager()
        {
            _fileManager = new XmlFileManager();
        }

        private XmlFileManager _fileManager;
        private ConsoleMenu _actionManager;

        protected override bool ExecuteCommand(char cmd)
        {
            switch (cmd)
            {
                case '0':
                    return true;
                case '1':
                    _fileManager.Menu();
                    return false;
                case '2':
                    _actionManager = new XmlDataSearchManager(_fileManager);
                    break;
                case '3':
                    _actionManager = new XmlNodeAccessManager(_fileManager);
                    break;
                case '4':
                    _actionManager = new XmlDataChangeManager(_fileManager);
                    break;
                default:
                    return IncorrectCommandOutput();                
            }

            _actionManager.Menu();
            return false;
        }

        protected override void WriteCommandList()
        {
            Console.WriteLine("== Главное меню ==");
            Console.WriteLine("[1] Открыть документ");
            Console.WriteLine("[2] Поиск информации, содержащейся в документе");
            Console.WriteLine("[3] Доступ к содержимому узлов");
            Console.WriteLine("[4] Внесение изменений в документ");
            Console.WriteLine("[0] Выход");
        }
    }
}

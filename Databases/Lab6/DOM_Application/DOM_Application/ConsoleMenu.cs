﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOM_Application
{
    abstract class ConsoleMenu
    {
        public void Menu()
        {
            char cmd = ' ';
            do
            {
                Console.Clear();
                WriteCommandList();
                cmd = ReadUserCommand();
                if (ExecuteCommand(cmd))
                    cmd = '0';
            }
            while (cmd != '0');
        }

        private char ReadUserCommand()
        {
            Console.Write("-> ");
            char cmd = Console.ReadKey().KeyChar;
            Console.WriteLine();
            Console.WriteLine();
            return cmd;
        }

        protected bool IncorrectCommandOutput()
        {
            Console.WriteLine("Ошибка: некорректная команда.");
            Pause();
            return false;
        }

        protected void Pause()
        {
            Console.WriteLine();
            Console.WriteLine("Нажмите Enter, чтобы продолжить...");
            Console.ReadLine();
        }

        protected string GetStringData(string msg)
        {
            Console.WriteLine(msg);
            Console.Write("-> ");
            string data = Console.ReadLine();
            Console.WriteLine();
            return data;
        }

        abstract protected void WriteCommandList();
        abstract protected bool ExecuteCommand(char cmd);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DOM_Application
{
    class XmlDataChangeManager : ConsoleMenu
    {
        protected XmlDataChangeManager() { }

        public XmlDataChangeManager(XmlFileManager file)
        {
            _file = file;
        }

        private XmlFileManager _file;

        protected override bool ExecuteCommand(char cmd)
        {
            switch (cmd)
            {
                case '0':
                    return true;
                case '1':
                    DeleteData();
                    return false;
                case '2':
                    ChangeData();
                    return false;
                case '3':
                    AddElement();
                    return false;
                case '4':
                    AddAttibute();
                    return false;
                default:
                    return IncorrectCommandOutput();
            }
        }

        private void DeleteData()
        {
            int num = 0;
            if (Int32.TryParse(GetStringData("Введите номер дочернего узла документа, который хотите удалить."), out num))
            {
                XmlNode node = _file.Document.DocumentElement.ChildNodes[num];
                if (node != null)
                {
                    _file.Document.DocumentElement.RemoveChild(node);
                    Console.WriteLine("Элемент удален.");
                }
                else
                    Console.WriteLine("В документе нет {0} дочернего узла:", num);
            }
            else
                Console.WriteLine("Ошибка: Вводимое значение должно быть целым числом.");

            Pause();
        }

        private void ChangeData()
        {
            string xPath = GetStringData("Введите выражение Xpath.");
            XmlNodeList list = _file.Document.SelectNodes(xPath);
            string newValue = GetStringData("Введите новое значение для выбранных узлов.");
            foreach (XmlNode node in list)
                node.Value = newValue;
            Console.WriteLine("Изменения внесены.");
            Pause();
        }

        private void AddElement()
        {
            XmlElement element = CreateElement();
            InsertElement(element);
            Console.WriteLine("Элемент добавлен.");
            Pause();
        }

        private void InsertElement(XmlElement element)
        {
            int max = _file.Document.DocumentElement.ChildNodes.Count - 1;
            string msg = string.Format("Введите номер дочернего узла документа, перед которым хотите поместить новый элемент. " +
                "Введите 0, чтобы поместить элемент в начало документа или значение больше общего количества элементов ({0}), " +
                "чтобы поместить его в конец документа.", max);
            int pos = (int)GetUIntData(msg);
            if (pos == 0)
                _file.Document.DocumentElement.PrependChild(element);
            else if (pos > max)
                _file.Document.DocumentElement.AppendChild(element);
            else
                _file.Document.DocumentElement.InsertBefore(element, _file.Document.DocumentElement.ChildNodes[pos]);
        }

        private XmlElement CreateElement()
        {
            string name = GetSpacelessString("Введите имя нового элемента (без пробелов).");
            var element = _file.Document.CreateElement(name);
            CreateChildren(element);
            return element;
        }

        private void CreateChildren(XmlElement element)
        {
            uint num = GetUIntData("Введите количество дочерних элементов.");
            for (uint i=0; i<num; i++)
            {
                string name = GetSpacelessString("Введите имя нового дочернего узла.");
                string data = GetStringData("Введите текстовое содержание дочернего узла.");
                XmlNode node = _file.Document.CreateElement(name);
                node.AppendChild(_file.Document.CreateTextNode(data));
                element.AppendChild(node);
            }
        }

        private uint GetUIntData(string msg)
        {
            uint value = 0;
            bool success = false;
            do
            {
                success = uint.TryParse(GetStringData(msg), out value);
                if (!success)
                    Console.WriteLine("Ошибка: значение должно быть неотрицательным целым числом.");
            }
            while (!success);
            return value;
        }

        private string GetSpacelessString(string msg)
        {
            string data = "";
            bool haveSpaces = false;
            do
            {
                data = GetStringData(msg);
                haveSpaces = data.Contains(" ");
                if (haveSpaces)
                    Console.WriteLine("Ошибка: в строке не должно быть пробелов.");
            }
            while (haveSpaces);
            return data;
        }

        private void AddAttibute()
        {
            int num = 0;
            if (Int32.TryParse(GetStringData("Введите номер дочернего узла документа, которому нужно добавить аттрибут"), out num))
            {
                XmlNode node = _file.Document.DocumentElement.ChildNodes[num];
                if (node != null)
                {
                    if (node.NodeType == XmlNodeType.Element)
                    { 
                        string name = GetSpacelessString("Введите имя аттрибута.");
                        string value = GetStringData("Введите значение аттрибута.");
                        XmlElement element = (XmlElement)node;
                        element.SetAttribute(name, value);
                        Console.WriteLine("Аттрибут добавлен.");
                    }
                    else
                        Console.WriteLine("Ошибка: узел не является элементом.");
                }
                else
                    Console.WriteLine("В документе нет {0} дочернего узла:", num);
            }
            else
                Console.WriteLine("Ошибка: Вводимое значение должно быть целым числом.");

            Pause();
        }

        protected override void WriteCommandList()
        {
            Console.WriteLine("== Внесение изменений в документ ==");
            Console.WriteLine("[1] Удаление содержимого");
            Console.WriteLine("[2] Внесение изменений в содержимое");
            Console.WriteLine("[3] Вставка (добавление) содержимого");
            Console.WriteLine("[4] Добавление атрибутов");
            Console.WriteLine("[0] Назад");
        }
    }
}

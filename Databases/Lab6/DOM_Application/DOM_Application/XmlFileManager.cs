﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DOM_Application
{
    class XmlFileManager : ConsoleMenu
    {
        public XmlFileManager()
        {
            Document = new XmlDocument();
            ReadFileName = null;
            WriteFileName = null;
        }

        public string ReadFileName { get; private set; }
        public XmlDocument Document { get; private set; }
        public string WriteFileName { get; private set; }

        private void LoadDocument()
        {
            Console.WriteLine();
            try
            {
      
                Document.Load(ReadFileName);
                Console.WriteLine("Документ успешно загружен из файла: " + ReadFileName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка: " + e.Message);
                ReadFileName = null;
            }
        }

        private void SaveDocument()
        {
            Console.WriteLine();
            try
            {
                Document.Save(WriteFileName);
                Console.WriteLine("Документ успешно сохранен в файл: " + WriteFileName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка: " + e.Message);
            }
        }

        private void SetWriteFile()
        {
            Console.WriteLine("Введите имя или путь к файлу.");
            Console.Write("-> ");
            WriteFileName = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Файл задан.");
            Pause();
        }


        private void WriteCurrentState()
        {
            Console.Write("Текущий файл чтения: ");
            if (ReadFileName == null)
                Console.WriteLine("<Не задан>");
            else
                Console.WriteLine(ReadFileName);

            Console.Write("Текущий файл записи: ");
            if (WriteFileName == null)
                Console.WriteLine("<Не задан>");
            else
                Console.WriteLine(WriteFileName);
            Pause();
        }

        private void SetReadFile()
        {
            Console.WriteLine("Введите имя или путь к файлу.");
            Console.Write("-> ");
            ReadFileName = Console.ReadLine();
            LoadDocument();
            Pause();
        }

        protected override void WriteCommandList()
        {
            Console.WriteLine("== Открыть документ ==");
            Console.WriteLine("[1] Открыть новый файл для чтения");
            Console.WriteLine("[2] Вывести текущее состояние");
            Console.WriteLine("[3] Указать файл для записи");
            Console.WriteLine("[4] Сохранить изменения");
            Console.WriteLine("[0] Назад");
        }

        protected override bool ExecuteCommand(char cmd)
        {
            switch (cmd)
            {
                case '0':
                    return true;
                case '1':
                    SetReadFile();
                    return false;
                case '2':
                    WriteCurrentState();
                    return false;
                case '3':
                    SetWriteFile();
                    return false;
                case '4':
                    SaveChanges();
                    return false;
                default:
                    return IncorrectCommandOutput();
            }
        }

        private void SaveChanges()
        {
            
            if (ReadFileName != null)
            {
                if (WriteFileName != null)
                {
                    SaveDocument();
                }
                else
                    Console.WriteLine("Ошибка: файл записи не задан.");
            }
            else
                Console.WriteLine("Ошибка: документ не был считан из файла.");

            Pause();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace DOM_Application
{
    class XmlDataSearchManager : ConsoleMenu
    {
        protected XmlDataSearchManager() { }

        public XmlDataSearchManager(XmlFileManager file)
        {
            _file = file;
        }

        private XmlFileManager _file;

        protected override bool ExecuteCommand(char cmd)
        {
            switch (cmd)
            {
                case '0':
                    return true;
                case '1':
                    GetElementsByTagNameMethod();
                    return false;
                case '2':
                    GetElementsByIdMethod();
                    return false;
                case '3':
                    SelectNodesMethod();
                    return false;
                case '4':
                    SelectSingleNodeMethod();
                    return false;
                default:
                    return IncorrectCommandOutput();
            }
        }

        private void SelectSingleNodeMethod()
        {
            string xPath = GetStringData("Введите выражение Xpath.");
            XmlNode node = _file.Document.SelectSingleNode(xPath);
            if (node != null)
                Console.WriteLine("Найденный узел:\r\n\t{0}: {1}", node.Name, node.InnerText);
            else
                Console.WriteLine("Для данного выражения в документе не найдено ни одного узла.");
            Pause();
        }

        private void SelectNodesMethod()
        {
            string xPath = GetStringData("Введите выражение Xpath.");
            XmlNodeList list = _file.Document.SelectNodes(xPath);
            if (list.Count != 0)
            {
                Console.WriteLine("Найденные узлы:");
                foreach (XmlNode node in list)
                    Console.WriteLine("\t{0}: {1}", node.Name, node.InnerText);
            }
            else
                Console.WriteLine("Для данного выражения в документе не найдено ни одного узла.");
            Pause();
        }

        private void GetElementsByIdMethod()
        {
            string id = GetStringData("Введите искомый ID");
            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                settings.ValidationType = ValidationType.DTD;

                var doc = new XmlDocument();
                using (XmlReader reader = XmlReader.Create(_file.ReadFileName, settings))
                {
                    doc.Load(reader);
                }

                XmlElement element = doc.GetElementById(id);
                if (element != null)
                {
                    Console.WriteLine("Содержимое элемента с ID \"{0}\":", id);
                    foreach (XmlNode node in element.ChildNodes)
                        Console.WriteLine("\t{0}: {1}", node.Name, node.InnerText);
                }
                else
                    Console.WriteLine("Элемент с ID \"{0}\" отсутствует в документе.", id);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка: " + e.Message);
            }
            finally
            {
                Pause();
            }
        }

        private void GetElementsByTagNameMethod()
        {
            string tagName = GetStringData("Введите имя тэга");
            XmlNodeList list = _file.Document.GetElementsByTagName(tagName);
            if (list.Count > 0)
            {
                Console.WriteLine("Найденные элементы с тэгом \"{0}\":", tagName);
                foreach (XmlNode node in list)
                    Console.WriteLine("\t" + node.InnerText);
            }
            else
            {
                Console.WriteLine("Элементы с тэгом \"{0}\" в документе не найдены.", tagName);
            }
            Pause();
        }

        protected override void WriteCommandList()
        {
            Console.WriteLine("== Поиск информации ==");
            Console.WriteLine("[1] Метод GetElementsByTagName");
            Console.WriteLine("[2] Метод GetElementsById");
            Console.WriteLine("[3] Mетод SelectNodes");
            Console.WriteLine("[4] Метод SelectSingleNode");
            Console.WriteLine("[0] Назад");
        }
    }
}

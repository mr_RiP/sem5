-- ������ �������
-- ��7-51
-- ������� 7

USE [Family]
GO

-- 1. �������� ��� �������� � ��������, ������� ������� �� �������� � �����, �� ������� ���� ������. 
SELECT *
FROM [Person]
WHERE [PersonID] NOT IN
(
	SELECT [HusbandID] 
	FROM [Marriage]
)
AND [PersonID] IN
(
	SELECT DISTINCT [FatherID]
	FROM [Person]
	WHERE [FatherID] IS NOT NULL
)
GO

-- 2. �������� ��� �������� � ��� ��������, ��� �������� �� �������� � �����. 
SELECT *
FROM [Person]
WHERE [FatherID] IS NOT NULL AND [MotherID] IS NOT NULL
	AND [FatherID] NOT IN
	(
		SELECT [HusbandID]
		FROM [Marriage]
	)
	AND [MotherID] NOT IN
	(
		SELECT [WifeID]
		FROM [Marriage]
	)
GO

-- 3. ��� ������ ���� ��������� ������� ���������� �� �����.
-- ������ ������ ��������: ��� � ������� ����, ��� � ������� ������, ���������� �����.
WITH [Parents] AS
(
	SELECT DISTINCT
		[FatherID],
		[MotherID],
		COUNT([PersonID]) AS [ChildrenCount]
	FROM [Person]
	WHERE [Person].[FatherID] IS NOT NULL AND [Person].[MotherID] IS NOT NULL
	GROUP BY [FatherID], [MotherID]
)
SELECT
	([Fathers].[FirstName] + ' ' + [Fathers].[LastName]) AS [Father],
	([Mothers].[FirstName] + ' ' + [Mothers].[LastName]) AS [Mother],
	[Parents].[ChildrenCount] AS [ChildrenCount]
FROM [Parents] INNER JOIN [Person] AS [Fathers] ON [Parents].[FatherID] = [Fathers].[PersonID]
	INNER JOIN [Person] AS [Mothers] ON [Parents].[MotherID] = [Mothers].[PersonID]
ORDER BY [ChildrenCount] DESC
GO
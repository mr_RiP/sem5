USE [Timetable]
GO

CREATE ASSEMBLY DBLib
FROM
'D:\Documents\sem5\Databases\Lab4\DBLib\DBLib\bin\Debug\DBLib.dll'
GO

CREATE FUNCTION GetOddNumber ( @number int )
RETURNS INT
AS
EXTERNAL NAME
DBLib.[DBLib.UserDefinedFunctions].GetOddNumber
GO

CREATE AGGREGATE MostFrequent ( @value int )
RETURNS int
EXTERNAL NAME
DBLib.[DBLib.UserDefinedAggregate.MostFrequent]
GO

CREATE FUNCTION FindSubstring ( @str nvarchar(max), @substr nvarchar(max) )
RETURNS TABLE
(
	[Position] int,
	[View] nvarchar(max)
)
AS
EXTERNAL NAME
DBLib.[DBLib.UserDefinedFunctions].FindSubstring
GO

CREATE PROCEDURE dbo.GetTeachersWorkingDay ( @day int )
AS
EXTERNAL NAME
DBLib.[DBLib.UserDefinedProcedures].GetTeachersWorkingDay
GO

CREATE TRIGGER [TRG_ClassesInsert] ON [Classes]
INSTEAD OF INSERT
AS
EXTERNAL NAME
DBLib.[DBLib.UserDefinedTriggers].ClassesInsert
GO

CREATE TYPE [WeekDay]
EXTERNAL NAME
DBLib.[DBLib.UserDefinedTypes.WeekDay]
GO
USE [Timetable]
GO

DROP FUNCTION dbo.GetOddNumber
GO

DROP AGGREGATE dbo.MostFrequent
GO

DROP FUNCTION dbo.FindSubstring
GO

DROP PROCEDURE dbo.GetTeachersWorkingDay
GO

DROP TRIGGER TRG_ClassesInsert
GO

DROP TYPE dbo.[WeekDay]
GO

DROP ASSEMBLY DBLib
GO
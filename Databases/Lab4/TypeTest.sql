USE [Timetable]
GO

CREATE TABLE [WeekDays]
(
	[Day Number] int NOT NULL,
	[Week Day] dbo.WeekDay NULL
)
GO

INSERT [WeekDays] ([Day Number], [Week Day])
VALUES (1, N'�����������')
INSERT [WeekDays] ([Day Number], [Week Day])
VALUES (3, N'�����')
INSERT [WeekDays] ([Day Number], [Week Day])
VALUES (5, N'5')
INSERT [WeekDays] ([Day Number], [Week Day])
VALUES (9, N'9')

SELECT [Day Number], [Week Day].ToString() AS [Week Day]
FROM [WeekDays]
GO

DROP TABLE [WeekDays]
GO
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using System.Data.SqlTypes;

namespace DBLib
{
    namespace UserDefinedAggregate
    {
        [Serializable]
        [SqlUserDefinedAggregate(Format.UserDefined, MaxByteSize = 8000)]
        public class MostFrequent : IBinarySerialize
        {
            public Dictionary<SqlInt32, int> Counter { get; private set; }

            public void Init()
            {
                Counter = new Dictionary<SqlInt32, int>();
            }

            public void Accumulate(SqlInt32 value)
            {
                if (Counter.ContainsKey(value))
                    Counter[value]++;
                else
                    Counter.Add(value, 1);
            }

            public void Merge(MostFrequent value)
            {
                foreach (KeyValuePair<SqlInt32, int> kvp in value.Counter)
                {
                    if (Counter.ContainsKey(kvp.Key))
                        Counter[kvp.Key] += kvp.Value;
                    else
                        Counter.Add(kvp.Key, kvp.Value);
                }
            }

            public SqlInt32 Terminate()
            {
                var max = new KeyValuePair<SqlInt32, int>(SqlInt32.Null, 0);
                foreach (KeyValuePair<SqlInt32, int> kvp in Counter)
                {
                    if (kvp.Value > max.Value)
                        max = kvp;
                }
                return max.Key;
            }

            #region IBinarySerialize Members
            public void Read(BinaryReader r)
            {
                Counter = new Dictionary<SqlInt32, int>();
                Counter.Add(r.ReadInt32(), 1);
            }

            public void Write(BinaryWriter w)
            {
                if ((Counter != null) && (Counter.Count > 0))
                    foreach (KeyValuePair<SqlInt32, int> kvp in Counter)
                    {
                        w.Write((int)kvp.Key);
                    }
            }
            #endregion
        }
    }
}
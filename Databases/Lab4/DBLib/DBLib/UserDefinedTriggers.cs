﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{
    public class UserDefinedTriggers
    {
        [SqlTrigger(Name = "TRG_ClassesInsert", Target = "Classes", Event = "INSTEAD OF INSERT")]
        public static void ClassesInsert()
        {
            using (var connection = new SqlConnection("context connection = true"))
            {
                var conflict_table_select = new SqlCommand("SELECT [Classes].[Id] AS [Id], [Classes].[TimeId] AS [TimeId], [Classes].[Room] AS [Room] FROM " +
                    "[Classes] INNER JOIN [Inserted] ON [Classes].[TimeId] = [Inserted].[TimeId] AND [Classes].[Room] = [Inserted].[Room]", connection);
                var insert = new SqlCommand("INSERT [Classes] ( [TeacherId], [TimeId], [CourseId], [Type], [Room], [BeginDate], [EndDate] ) " +
                    "SELECT [TeacherId], [TimeId], [CourseId], [Type], [Room], [BeginDate], [EndDate] FROM [Inserted]", connection);
                var err_msg = new SqlCommand("PRINT N'Добавление невозможно по причине наличия конфликтующих значений.'", connection);

                bool conflict = false;

                connection.Open();
                using (SqlDataReader reader = conflict_table_select.ExecuteReader())
                {
                    conflict = reader.HasRows;
                }

                if (conflict)
                {
                    SqlContext.Pipe.ExecuteAndSend(err_msg);
                    SqlContext.Pipe.ExecuteAndSend(conflict_table_select);
                }
                else
                {
                    insert.ExecuteNonQuery();
                }
            }
        }
    }
}
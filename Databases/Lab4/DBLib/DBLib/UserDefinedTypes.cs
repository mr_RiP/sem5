﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{
    namespace UserDefinedTypes
    {
        [Serializable]
        [SqlUserDefinedType(Format.Native, IsByteOrdered = true)]
        public struct WeekDay : INullable
        {
            private bool _isnull;
            private int _day;

            WeekDay(int day)
            {
                if ((day > 0) && (day < 8))
                {
                    _day = day;
                    _isnull = false;
                }
                else
                {
                    _day = 1;
                    _isnull = true;
                }
            }

            WeekDay(bool isnull)
            {
                _isnull = isnull;
                _day = 1;
            }

            WeekDay(int day, bool isnull)
            {
                if ((day > 0) && (day < 8))
                {
                    _day = day;
                    _isnull = isnull;
                }
                else
                {
                    _day = 1;
                    _isnull = true;
                }
            }


            public bool IsNull
            {
                get
                {
                    return _isnull;
                }
            }

            public SqlInt32 DayNo
            {
                get
                {
                    return _day;
                }
                set
                {
                    if ((value.Value > 0) && (value.Value < 8))
                    {
                        _isnull = false;
                        _day = value.Value;
                    }
                    else
                        throw new ArgumentException("Invalid value for WeekDay DayNo. DayNo must be an integer value between 1 and 7.");
                }
            }

            public static WeekDay Null
            {
                get
                {
                    return new WeekDay(true);
                }
            }

            
            public override string ToString()
            {
                if (_isnull)
                    return null;
                else
                    switch (_day)
                    {
                        case 1: return "понедельник";
                        case 2: return "вторник";
                        case 3: return "среда";
                        case 4: return "четверг";
                        case 5: return "пятница";
                        case 6: return "суббота";
                        case 7: return "воскресенье";
                        default: return null;
                    }
            }

            public static WeekDay Parse(SqlString sql_str)
            {
                string str = sql_str.ToString().Trim();

                if (sql_str.IsNull || str == null)
                    return new WeekDay(true);

                int res = 1;
                if (Int32.TryParse(str, out res))
                    return new WeekDay(res);
                else
                    switch (str)
                    {
                        case "понедельник": return new WeekDay(1);
                        case "вторник": return new WeekDay(2);
                        case "среда": return new WeekDay(3);
                        case "четверг": return new WeekDay(4);
                        case "пятница": return new WeekDay(5);
                        case "суббота": return new WeekDay(6);
                        case "воскресенье": return new WeekDay(7);
                        default: return new WeekDay(true);
                    }
            }
        }
    }
}

﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{
    public class UserDefinedProcedures
    {
        [SqlProcedure]
        public static void GetTeachersWorkingDay(SqlInt32 day)
        {
            using (var connection = new SqlConnection("context connection = true"))
            {
                string sql_code = "SELECT * FROM [Teachers] WHERE [Id] IN ( SELECT DISTINCT [TeacherId] FROM " +
                    "[Classes] INNER JOIN [TimeFrames] ON [Classes].[TimeId] = [TimeFrames].[Id] WHERE [Day] = @day )";
                var command = new SqlCommand(sql_code, connection);

                command.Parameters.AddWithValue("@day", day);
                connection.Open();

                SqlContext.Pipe.ExecuteAndSend(command);
            }
        }
    }
}

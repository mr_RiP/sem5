﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBLib
{
    public class SubstringView
    {
        public SqlInt32 Position { get; set; }
        public SqlString View { get; set; }
    }
}

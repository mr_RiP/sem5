﻿using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections;

namespace DBLib
{
    public class UserDefinedFunctions
    {
        [SqlFunction]
        public static SqlInt32 GetOddNumber(SqlInt32 number)
        {
            if ((number % 2) != 0)
                return number - 1;
            else
                return number;
        }

        [SqlFunction(FillRowMethodName = "FindSubstringFillRow", TableDefinition = "[Position] int, [View] nvarchar(max)")]
        public static IEnumerable FindSubstring(SqlString in_str, SqlString in_substr)
        {
            string str = in_str.ToString();
            string substr = in_substr.ToString();

            int mov = 0;
            string tmp = str;
            int index = -1;

            while ((index = tmp.IndexOf(substr)) >= 0)
            {
                int pos = mov + index;

                mov += ++index;
                tmp = tmp.Substring(index);

                string view = str;
                view = view.Insert(pos + substr.Length, "]");
                view = view.Insert(pos, "[");

                yield return new SubstringView { Position = pos, View = view };
            }
        }

        public static void FindSubstringFillRow(object row, out SqlInt32 pos, out SqlString view)
        {
            pos = ((SubstringView)row).Position;
            view = ((SubstringView)row).View;
        }
    }
}

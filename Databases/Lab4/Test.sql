USE [Timetable]
GO

SELECT dbo.GetOddNumber(4) AS [Odd From 4]
GO
SELECT dbo.GetOddNumber(3) AS [Odd From 3]
GO

SELECT dbo.MostFrequent([TeacherId]) AS [Most frequent teacher from Classes]
FROM [Classes]
GO

SELECT *
FROM dbo.FindSubstring(N'Address restored', N'res')
GO

EXECUTE dbo.GetTeachersWorkingDay 3
GO

INSERT [Classes] ( [CourseId], [TeacherId], [TimeId], [Room], [Type], [BeginDate], [EndDate])
VALUES ( 1, 1, 93, N'615�', N'�������', '2010-09-01', '2010-12-24')
GO
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToObjects
{
    public class Teacher
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string AcademicRank { get; set; }
        public string Email { get; set; }
        public int DepartmentId { get; set; }
        public int? SupervisorId { get; set; }

        public override string ToString()
        {
            return string.Join
            (
                " ",
                Id.ToString(),
                FirstName,
                MiddleName,
                LastName,
                AcademicRank,
                Email,
                DepartmentId.ToString(),
                SupervisorId.ToString()
            );
        }

        public static List<Teacher> GenerateList()
        {
            var data = new List<Teacher>
            {
                new Teacher { Id = 1, FirstName = "Игорь", MiddleName = "Владимирович", LastName = "Рудаков", DepartmentId = 1, AcademicRank = "доцент", },
                new Teacher { Id = 2, FirstName = "Валерий", MiddleName = "Николаевич", LastName = "Ремарчук", DepartmentId = 5, AcademicRank = "профессор" },
                new Teacher { Id = 3, FirstName = "Евгений", MiddleName = "Алексеевич", LastName = "Просуков", DepartmentId = 1, AcademicRank = "доцент", SupervisorId = 1 },
                new Teacher { Id = 4, FirstName = "Михаил", MiddleName = "Васильевич", LastName = "Ульянов", DepartmentId = 1 , AcademicRank = "профессор", Email = "muljanov@mail.ru", SupervisorId = 1 },
                new Teacher { Id = 5, FirstName = "Лидия", MiddleName = "Леонидовна", LastName = "Волкова", DepartmentId = 1, AcademicRank = "доцент", Email = "lvolkova@hse.ru", SupervisorId = 1 },
                new Teacher { Id = 6, FirstName = "Наталья", MiddleName = "Юрьевна", LastName = "Рязанова", DepartmentId = 1, AcademicRank = "доцент", Email = "ryaz_nu@mail.ru", SupervisorId = 1 },
                new Teacher { Id = 7, FirstName = "Павел", MiddleName = "Александрович", LastName = "Власов", DepartmentId = 3, AcademicRank = "доцент", Email = "pvlx@mail.ru" },
                new Teacher { Id = 8, FirstName = "Юрий", MiddleName = "Владимирович", LastName = "Синчук", DepartmentId = 5, AcademicRank = "профессор", SupervisorId = 2 },
                new Teacher { Id = 9, FirstName = "Андрей", MiddleName = "Владимирович", LastName = "Куров", DepartmentId = 1, AcademicRank = "доцент", SupervisorId = 1 },
                new Teacher { Id = 10, FirstName = "Алексей", MiddleName = "Юрьевич", LastName = "Попов", DepartmentId = 2, AcademicRank = "доцент", Email = "alexpopov@bmstu.ru" },
                new Teacher { Id = 11, FirstName = "Ольга", MiddleName = "Ивановна", LastName = "Комарова", DepartmentId = 4, AcademicRank = "старший преподаватель", Email = "o.i.komarova@mail.ru" },
                new Teacher { Id = 12, FirstName = "Анатолий", MiddleName = "Васильевич", LastName = "Ореховский", DepartmentId = 5, AcademicRank = "доцент", SupervisorId = 2 }
            };
            return data;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Teacher> teachers = Teacher.GenerateList();

            PrintTeachers(teachers, "Все преподаватели:");
            // Filtering - преподаватели с каф. ИУ-7 (DepartmentId == 1)
            PrintTeachers(UI7Teachers(teachers), "1. Преподаватели с кафедры ИУ-7 (DepartmentId=1), cортировка по фамилии.");
            // Filtering - преподаватели с электронной почтой на Mail.ru
            PrintTeachers(MailRuTeachers(teachers), "2. Преподаватели с электронной почтой @mail.ru, обратная сортировка по Id.");
            // GroupBy - кол-во преподавателей для каждой кафедры
            PrintDepartmentsStats(teachers);
            // Join - преподаватели с одинаковым отчеством
            PrintSameMiddleNameTeachers(teachers);
            // Concat (аналог UNION ALL) - цепочка старшинства с рангом
            PrintTeachersWithDepartmentRank(teachers);
        }

        private static void PrintTeachersWithDepartmentRank(IEnumerable<Teacher> teachers)
        {
            var ranked = teachers.Select(t => new { Teacher = t, Rank = 1 });
            var result = ranked.Where(t => t.Teacher.SupervisorId == null);

            do
            {
                result = result.Concat(ranked
                    .Where(t => result.Select(r => (int?)r.Teacher.Id).Contains(t.Teacher.SupervisorId))
                    .Select(t => new { Teacher = t.Teacher, Rank = t.Rank + 1 })).ToList();
            }
            while (ranked.Count() != result.Count());
            result = result.OrderBy(r => r.Rank);

            Console.WriteLine("5. Преподаватели с указанием кафедрального ранга.");
            foreach (var r in result)
            {
                Teacher t = r.Teacher;
                Console.WriteLine("\t[{0}]\t (каф. {4} ранг {5})\t {1} {2} {3}", t.Id, t.LastName, t.FirstName, t.MiddleName, t.DepartmentId, r.Rank);
            }
            Pause();
        }

        private static IEnumerable<Teacher> MailRuTeachers(IEnumerable<Teacher> teachers)
        {
            return teachers.Where(t => t.Email != null && t.Email.Contains("mail.ru")).OrderByDescending(t => t.Id);
        }

        private static void PrintSameMiddleNameTeachers(IEnumerable<Teacher> teachers)
        {
            var list = teachers.Join(teachers,
                t1 => t1.MiddleName, t2 => t2.MiddleName,
                (t1, t2) => new
                {
                    FirstTeacherId = t1.Id,
                    FirstTeacherName = string.Join(" ", t1.LastName, t1.FirstName, t1.MiddleName),
                    SecondTeacherId = t2.Id,
                    SecondTeacherName = string.Join(" ", t2.LastName, t2.FirstName, t2.MiddleName)
                })
                .Where(t => t.FirstTeacherId < t.SecondTeacherId);
            Console.WriteLine("4. Преподаватели с одинаковым отчеством.");
            foreach (var t in list)
                Console.WriteLine("\t[{0}] {1} : [{2}] {3}", t.FirstTeacherId, t.FirstTeacherName, t.SecondTeacherId, t.SecondTeacherName);
            Pause();
        }

        private static void PrintDepartmentsStats(IEnumerable<Teacher> teachers)
        {
            var list = teachers
                .GroupBy(t => t.DepartmentId)
                .Select(c => new { DepartmentId = c.Key, Count = c.Count() })
                .OrderBy(c => c.DepartmentId);
            Console.WriteLine("3. Количество преподавателей на кафедрах.");
            Console.WriteLine("DepartmentId Count");
            foreach (var t in list)
                Console.WriteLine("\t{0}\t{1}", t.DepartmentId, t.Count);
            Pause();
        }

        private static void PrintTeachers(IEnumerable<Teacher> teachers, string header)
        {
            Console.WriteLine(header);
            foreach (Teacher t in teachers)
                Console.WriteLine("\t" + t.ToString());
            Pause();
        }

        private static void Pause()
        {
            Console.WriteLine();
            Console.WriteLine("Нажмите Enter, чтобы продолжить...");
            Console.ReadLine();
        }

        private static IEnumerable<Teacher> UI7Teachers(IEnumerable<Teacher> teachers)
        {
            return teachers.Where(t => t.DepartmentId == 1).OrderBy(t => t.LastName);
        }
    }
}

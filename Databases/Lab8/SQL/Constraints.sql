USE [Timetable]
GO

ALTER TABLE [dbo].[Departments] ADD 
CONSTRAINT [PK_Departments] PRIMARY KEY ([Id]),
CONSTRAINT [UK_Departments] UNIQUE ([Faculty], [Index]),
CONSTRAINT [CHK_Departments_Index] CHECK ([Index] > 0)
GO

ALTER TABLE [dbo].[Teachers] ADD 
CONSTRAINT [PK_Teachers] PRIMARY KEY ([Id]),
CONSTRAINT [FK_Teachers_Departments] FOREIGN KEY([DepartmentId]) REFERENCES [dbo].[Departments] ([Id]),
CONSTRAINT [FK_Teachers_Teachers] FOREIGN KEY([SupervisorId]) REFERENCES [dbo].[Teachers] ([Id])
GO

ALTER TABLE [dbo].[Courses] ADD 
CONSTRAINT [PK_Courses] PRIMARY KEY ([Id]),
CONSTRAINT [UK_Courses] UNIQUE ([Name])
GO

ALTER TABLE [dbo].[TimeFrames] ADD
CONSTRAINT [PK_TimeFrames] PRIMARY KEY ([Id]),
CONSTRAINT [CHK_TimeFrames_Time] CHECK ([Begin] < [End]),
CONSTRAINT [CHK_TimeFrames_Day] CHECK ([Day] BETWEEN 1 AND 7) 
GO

ALTER TABLE [dbo].[Classes] ADD 
CONSTRAINT [PK_Classes] PRIMARY KEY ([Id]),
CONSTRAINT [FK_Classes_Teachers] FOREIGN KEY([TeacherId]) REFERENCES [dbo].[Teachers] ([Id]),
CONSTRAINT [FK_Classes_TimeFrames] FOREIGN KEY([TimeId]) REFERENCES [dbo].[TimeFrames] ([Id]),
CONSTRAINT [FK_Classes_Courses] FOREIGN KEY([CourseId]) REFERENCES [dbo].[Courses] ([Id]),
CONSTRAINT [UK_Classes_Time_Room] UNIQUE ([TimeId],[Room])
GO
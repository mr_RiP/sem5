USE [Timetable]
GO

IF OBJECT_ID(N'dbo.GetDepartmentHierarchy') IS NOT NULL
DROP PROCEDURE [dbo].[GetDepartmentHierarchy]
GO

CREATE PROCEDURE [dbo].[GetDepartmentHierarchy] (@department_id int)
AS
BEGIN
	WITH [Hierarchy] AS
	(
		SELECT
			[Id],
			[FirstName], 
			[MiddleName],
			[LastName],
			[AcademicRank],
			1 AS [DepartmentRank]
		FROM [Teachers]
		WHERE [DepartmentId] = @department_id AND [SupervisorId] IS NULL
	
		UNION ALL
		SELECT 
			[Teachers].[Id] AS [Id],
			[Teachers].[FirstName] AS [FirstName],
			[Teachers].[MiddleName] AS [MiddleName],
			[Teachers].[LastName] AS [LastName],
			[Teachers].[AcademicRank] AS [AcademinRank],
			[Hierarchy].[DepartmentRank] + 1 AS [DepartmentRank]
		FROM [Teachers] INNER JOIN [Hierarchy] ON [Teachers].[SupervisorId] = [Hierarchy].[Id]
	)
	SELECT *
	FROM [Hierarchy]
	ORDER BY [DepartmentRank] ASC
END;
GO
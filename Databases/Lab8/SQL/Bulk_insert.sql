USE [Timetable]
GO

BULK INSERT [Timetable].[dbo].[Departments]
FROM 'D:\Documents\sem5\Databases\Lab8\SQL\Data\Departments.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[Teachers]
FROM 'D:\Documents\sem5\Databases\Lab8\SQL\Data\Teachers.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[Courses]
FROM 'D:\Documents\sem5\Databases\Lab8\SQL\Data\Courses.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[TimeFrames]
FROM 'D:\Documents\sem5\Databases\Lab8\SQL\Data\TimeFrames.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO

BULK INSERT [Timetable].[dbo].[Classes]
FROM 'D:\Documents\sem5\Databases\Lab8\SQL\Data\Classes.txt'
WITH (DATAFILETYPE = 'widechar', FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n');
GO
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace LinqToSql.Model
{
    [Table(Name = "TimeFrames")]
    public class TimeFrame
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column]
        public int Day { get; set; }
        [Column]
        public DateTime Begin { get; set; }
        [Column]
        public DateTime End { get; set; }
        [Column]
        public bool Nominator { get; set; }
        [Column]
        public bool Denominator { get; set; }
        [Column]
        public string Description { get; set; }
    }
    
}
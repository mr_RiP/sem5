﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToSql.Model
{
    [Table(Name = "Teachers")]
    public class Teacher
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column]
        public string FirstName { get; set; }
        [Column]
        public string LastName { get; set; }
        [Column]
        public string MiddleName { get; set; }
        [Column]
        public string AcademicRank { get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public int DepartmentId { get; set; }
        [Column]
        public int? SupervisorId { get; set; }
    } 
}

﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToSql.Model
{
    [Table(Name = "Classes")]
    public class Class
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column]
        public int TeacherId { get; set; }
        [Column]
        public int TimeId { get; set; }
        [Column]
        public int CourseId { get; set; }
        [Column]
        public string Type { get; set; }
        [Column]
        public string ClassRoom { get; set; }
    }
}

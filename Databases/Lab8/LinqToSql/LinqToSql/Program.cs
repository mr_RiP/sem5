﻿using LinqToSql.Model;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Console;

namespace LinqToSql
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=localhost;Initial Catalog=Timetable;Integrated Security=True";
            using (var db = new TimetableDataContext(connectionString))
            {
                SingleTableRequest(db);
                MultiTableRequest(db);
                ChangeRequest(db);
                ProcedureRequest(db);            
            }
        }

        private static void ProcedureRequest(TimetableDataContext db)
        {
            var result = db.GetDepartmentHierarchy(1);
            WriteLine("4. Используя хранимую процедуру 'dbo.GetDepartmentHierarchy' получаем иерархическую таблицу кафедры ИУ-7.");
            foreach (var t in result)
                WriteLine("\t[{0}] (Rank {1}) {2} {3} {4}, {5}", t.Id, t.DepartmentRank, t.LastName, t.FirstName, t.MiddleName, t.AcademicRank);
            Pause();
        }

        private static void ChangeRequest(TimetableDataContext db)
        {
            WriteCourses(db, "4.0. Таблица учебных курсов.");
            AddRequest(db);
            UpdateRequest(db);
            DeleteRequest(db);
        }

        private static void DeleteRequest(TimetableDataContext db)
        {
            var element = db.Courses.Where(t => t.Name == "Системное программирование").First();
            db.Courses.DeleteOnSubmit(element);
            db.SubmitChanges();

            WriteCourses(db, "4.3. А теперь удалим его:");
        }

        private static void UpdateRequest(TimetableDataContext db)
        {
            db.Courses.Where(t => t.Name == "Компьютерная графика").First().Name = "Системное программирование";
            db.SubmitChanges();

            WriteCourses(db, "4.2. Изменим его имя на 'Системное программирование':");
        }

        private static void WriteCourses(TimetableDataContext db, string msg)
        {
            WriteLine(msg);
            foreach (var t in db.Courses)
                WriteLine("\t[{0}] \t{1}", t.Id, t.Name);
            Pause();
        }

        private static void AddRequest(TimetableDataContext db)
        {
            var newCourse = new Course { Name = "Компьютерная графика" };
            db.Courses.InsertOnSubmit(newCourse);
            db.SubmitChanges();

            WriteCourses(db, "4.1. Добавим к ним курс 'Компьютерная графика':");
        }

        private static void MultiTableRequest(TimetableDataContext db)
        {
            var selection = db.Classes.Join(db.TimeFrames.Where(t => (t.Day == 5) && (t.Nominator == true)),
                cl => cl.TimeId, tf => tf.Id,
                (cl, tf) => new { Id = cl.Id, Time = tf.Begin, Type = cl.Type, CourseId = cl.CourseId })
                .Join(db.Courses,
                cltf => cltf.CourseId, co => co.Id,
                (cltf, co) => new { Id = cltf.Id, Time = cltf.Time, Course = co.Name, Type = cltf.Type })
                .ToList();
            
            WriteLine("2. Выведем распиание на пятницу (числитель), объединив данный из таблиц Classes, TimeFrames и Courses.");
            foreach (var t in selection)
                WriteLine("\t[{0}] {1} - {2} ({3})", t.Id, t.Time.TimeOfDay, t.Course, t.Type);
            Pause();
        }

        private static void SingleTableRequest(TimetableDataContext db)
        {
            var selection = db.Teachers.Where(t => t.DepartmentId == 1).ToList();
            WriteLine("1. Выберем всех преподавателей с кафедры ИУ-7 (DepartmentId = 1), отобразив иx Id, LastName и DepartmentId");
            foreach (var t in selection)
                WriteLine("\tId='{0}'  LastName='{1}'  DepartmentId='{2}'", t.Id, t.LastName, t.DepartmentId);
            Pause();
        }

        private static void Pause()
        {
            WriteLine();
            WriteLine("Нажмите Enter, чтобы продолжить...");
            ReadLine();
        }
    }
}

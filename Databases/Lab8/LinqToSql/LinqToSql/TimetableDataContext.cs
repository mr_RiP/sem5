﻿using LinqToSql.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToSql
{
    public class TimetableDataContext : DataContext
    {
        public TimetableDataContext(IDbConnection connection) : base(connection)
        {
        }

        public TimetableDataContext(string fileOrServerOrConnection) : base(fileOrServerOrConnection)
        {
        }

        public TimetableDataContext(IDbConnection connection, MappingSource mapping) : base(connection, mapping)
        {
        }

        public TimetableDataContext(string fileOrServerOrConnection, MappingSource mapping) : base(fileOrServerOrConnection, mapping)
        {
        }

        public Table<Class> Classes
        {
            get { return GetTable<Class>(); }
        }

        public Table<Course> Courses
        {
            get { return GetTable<Course>(); }
        }

        public Table<Department> Departments
        {
            get { return GetTable<Department>(); }
        }

        public Table<Teacher> Teachers
        {
            get { return GetTable<Teacher>(); }
        }

        public Table<TimeFrame> TimeFrames
        {
            get { return GetTable<TimeFrame>(); }
        }


        public class DepartmentHierarchy
        {
            [Column]
            public int Id { get; set; }
            [Column]
            public string FirstName { get; set; }
            [Column]
            public string MiddleName { get; set; }
            [Column]
            public string LastName { get; set; }
            [Column]
            public string AcademicRank { get; set; }
            [Column]
            public int DepartmentRank { get; set; }
        }

        [Function(Name = "dbo.GetDepartmentHierarchy")]
        public ISingleResult<DepartmentHierarchy> GetDepartmentHierarchy([Parameter(DbType = "int", Name = "@department_id")] int id)
        {
            var result = base.ExecuteMethodCall(this, (System.Reflection.MethodInfo)System.Reflection.MethodInfo.GetCurrentMethod(), id);
            return (ISingleResult<DepartmentHierarchy>)result.ReturnValue;
        }

    }
}

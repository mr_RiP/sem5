﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument doc = XDocument.Load(@"D:\Documents\sem5\Databases\Lab8\Teachers.xml");

            ShowAllData(doc);
            ShowSpecificData(doc);
            ModifyData(doc);
            AddData(doc);
            ShowAllData(doc);
        }

        private static void AddData(XDocument doc)
        {
            XElement newTeacher = new XElement("Teacher",
                new XElement("Id", "13"),
                new XElement("FirstName", "Игорь"),
                new XElement("MiddleName", "Владимирович"),
                new XElement("LastName", "Ломовской"),
                new XElement("AcademicRank", "старший преподаватель"),
                new XElement("DepartmentId", "1"),
                new XElement("SupervisorId", "1"));

            doc.Element("Teachers").Add(newTeacher);
            doc.Save(@"D:\Documents\sem5\Databases\Lab8\NewTeachers.xml");

            Console.WriteLine("Добавим к списку преподавателей Игоря Владимировича Ломовского.");
            Pause();
        }

        private static void ModifyData(XDocument doc)
        {
            var elements = doc.Element("Teachers")
                .Elements("Teacher")
                .Where(node => node.Element("MiddleName").Value.Equals("Владимирович"));

            foreach (var element in elements)
                element.Element("FirstName").SetValue("Игорь");

            WriteTeachersList(elements, "Изменим имя для всех преподавателей с отчеством \"Владимирович\" на \"Игорь\"");
            Pause();
        }

        private static void ShowAllData(XDocument doc)
        {
            var list = doc.Element("Teachers")
                .Elements("Teacher");

            WriteTeachersList(list, "Все преподаватели (лишние данные опущены).");
            Pause();
        }

        private static void ShowSpecificData(XDocument doc)
        {
            var list = doc.Element("Teachers")
                .Elements("Teacher")
                .Where(node => node.Element("DepartmentId").Value.Equals("1"));

            WriteTeachersList(list, "Преподаватели с кафедры ИУ-7 (ID кафедры = 1).");
            Pause();
        }

        private static void Pause()
        {
            Console.WriteLine();
            Console.WriteLine("Нажмите Enter, чтобы продолжить...");
            Console.ReadLine();
        }

        private static void WriteTeachersList(IEnumerable<XElement> elements, string msg)
        {
            var list = elements.Select(node => new
            {
                Id = node.Element("Id").Value,
                Name = string.Join(" ", node.Element("LastName").Value, node.Element("FirstName").Value, node.Element("MiddleName").Value),
                DepartmentId = node.Element("DepartmentId").Value
            }).ToList();

            Console.WriteLine(msg);
            foreach (var t in list)
                Console.WriteLine("\t[{0}] (ID кафедры: {2}) {1}", t.Id, t.Name, t.DepartmentId);
        }
    }
}

USE [Timetable]
GO

-- ������� ��������� Classes
SELECT *
FROM [Classes]
GO

-- �������� ���� ����� �� Classes, ��������� IDENTITY
DELETE FROM [Classes]
GO
DBCC CHECKIDENT ('[Classes]', RESEED, 0);
GO

-- ��������� Classes ����� ��������
SELECT *
FROM [Classes]
GO

-- �������� �� XML, �������� ������ � XML
DECLARE @idoc int
DECLARE @doc xml
SELECT @doc = c FROM OPENROWSET (BULK 'D:\Documents\sem5\Databases\Lab5\Task2\classes.xml', SINGLE_BLOB) AS TEMP(c)
EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
SET IDENTITY_INSERT [Classes] ON
SELECT *
FROM OPENXML (@idoc, '/Classes/Class', 1)
	WITH 
	(
		[Id] int, 
		[TeacherId] int, 
		[TimeId] int, 
		[CourseId] int, 
		[Type] nvarchar(32),
		[Room] nvarchar(16),
		[BeginDate] date, 
		[EndDate] date
	);
INSERT INTO [Classes]
(
	[Id],
	[TeacherId], 
	[TimeId], 
	[CourseId], 
	[Type],
	[Room], 
	[BeginDate], 
	[EndDate]
)
	SELECT *
	FROM OPENXML (@idoc, '/Classes/Class', 1)
		WITH 
		(
			[Id] int, 
			[TeacherId] int, 
			[TimeId] int, 
			[CourseId] int, 
			[Type] nvarchar(32),
			[Room] nvarchar(16),
			[BeginDate] date, 
			[EndDate] date
		);
SET IDENTITY_INSERT [Classes] OFF
EXEC sp_xml_removedocument @idoc;
GO 

-- �������� ������ Classes ����� ��������
SELECT *
FROM [Classes]
GO

USE [Timetable]
GO

SELECT *
FROM [Classes]
FOR XML 
	RAW(N'Class'), 
	ROOT(N'Classes'), 
	TYPE
GO
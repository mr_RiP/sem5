USE [Timetable]
GO

SELECT *
FROM [Teachers]
FOR XML 
	RAW(N'Teacher'), 
	ROOT(N'Teachers'), 
	TYPE,
	ELEMENTS
GO
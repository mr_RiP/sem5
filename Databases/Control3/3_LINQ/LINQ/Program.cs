﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = XDocument.Load(@"D:\Documents\sem5\Databases\Control3\XMLFile1.xml");
            var cs_books_authors = root.Element("Books")
                .Elements("Book")
                .Where(el => ((string)el.Element("Title")).Contains("C#"))
                .Elements("Authors")
                .Elements("Author");

            foreach (XElement el in cs_books_authors)
                Console.WriteLine(el.Value);
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DOM
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"D:\Documents\sem5\Databases\Control3\XMLFile1.xml");
            XmlNodeList nodeList;
            XmlNode root = doc.DocumentElement;
            nodeList = root.SelectNodes("Book");
            foreach (XmlNode book in nodeList)
                if (book["Title"].InnerText.Contains("C#"))
                {
                    XmlNodeList authors = book.SelectSingleNode("Authors").SelectNodes("Author");
                    foreach (XmlNode author in authors)
                        Console.WriteLine(author.InnerText);
                }
            Console.ReadLine();

        }
    }
}

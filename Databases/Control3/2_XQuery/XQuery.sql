DECLARE @xml xml;

SELECT @xml = x 
FROM OPENROWSET(BULK 'D:\Documents\sem5\Databases\Control3\XMLFile1.xml', SINGLE_BLOB) AS TEMP(x);

SELECT @xml.query('
for $b in /Books/Book
where contains(string($b/Title[1]), "C#")
return ($b/Authors/Author)
');
GO

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateApp
{
    // Программа заменяет значения [string_data] таблицы [SomeDataTable] в строках,
    // с указанным значением [int_data], на указанное пользователем.
    class Program
    {
        static void Main(string[] args)
        {
            string int_data = GetIntData();
            string string_data = GetStringData();

            UpdateTable(int_data, string_data);

            Console.ReadLine();
        }

        static string GetIntData()
        {
            Console.WriteLine("Введите int_data, для которого требуется изменить string_data.");
            return Console.ReadLine();
        }

        static string GetStringData()
        {
            Console.WriteLine("Введите желаемое значение str_data.");
            return Console.ReadLine();
        }

        static void UpdateTable(string int_data, string string_data)
        {
            string connection_string = @"Data Source=localhost;Initial Catalog=ControlDB;Integrated Security=True";
            var connection = new SqlConnection(connection_string);

            string query = @"UPDATE [SomeDataTable] SET [string_data] = @string_data WHERE [int_data] = @int_data";

            var command = new SqlCommand(query, connection);
            command.Parameters.Add("@string_data", SqlDbType.NVarChar, -1);
            command.Parameters.Add("@int_data", SqlDbType.Int);

            try
            {
                connection.Open();
                command.Parameters["@int_data"].Value = int_data;
                command.Parameters["@string_data"].Value = string_data;
                command.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.WriteLine("Ошибка обновления данных.");
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Данные обновлены.");
            }
        }
    }
}

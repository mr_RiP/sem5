﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeleteApp
{
    // Программа удаляет строки таблицы [SomeDataTable], значения [int_data] которых находятся между значениями, вводимыми пользователем.
    class Program
    {
        static void Main(string[] args)
        {
            string min = GetMinValue();
            string max = GetMaxValue();

            DeleteTableLines(min, max);

            Console.ReadLine();
        }

        static string GetMinValue()
        {
            Console.WriteLine("Введите минимальное значение int_data.");
            return Console.ReadLine();
        }

        static string GetMaxValue()
        {
            Console.WriteLine("Введите максимальное значение int_data.");
            return Console.ReadLine();
        }

        static void DeleteTableLines(string min, string max)
        {
            string connection_string = @"Data Source=localhost;Initial Catalog=ControlDB;Integrated Security=True";
            var connection = new SqlConnection(connection_string);

            string query = @"DELETE [SomeDataTable] WHERE [int_data] BETWEEN @min AND @max";

            var command = new SqlCommand(query, connection);
            command.Parameters.Add("@min", SqlDbType.Int);
            command.Parameters.Add("@max", SqlDbType.Int);

            try
            {
                connection.Open();
                command.Parameters["@min"].Value = min;
                command.Parameters["@max"].Value = max;
                command.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.WriteLine("Ошибка удаления данных.");
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();
                Console.WriteLine("Данные удалены.");
            }
        }
    }
}

-- �������� �������� ��
USE [master]
GO

IF EXISTS (SELECT name FROM sys.databases WHERE name = N'ControlDB')
	DROP DATABASE [ControlDB]
GO

CREATE DATABASE [ControlDB]
GO

USE [ControlDB]
GO

-- �������� �������� �������
CREATE TABLE [dbo].[SomeDataTable]
(
	[int_data] int NOT NULL,
	[string_data] nvarchar(MAX) NOT NULL
)
GO

-- ���� �������� ������
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (1, N'����');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (2, N'���');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (3, N'���');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (4, N'������');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (5, N'����');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (6, N'�����');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (7, N'����');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (8, N'������');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (9, N'������');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (10, N'������');
GO

-- �������� ������� ������
CREATE TABLE [dbo].[Audit]
(	
	[int_data] int NOT NULL,
	[string_data] nvarchar(MAX) NOT NULL,
	[action] char(6) NOT NULL,
	[date_changed] datetime NOT NULL,
	[changed_by_user_name] sysname NOT NULL,

	CONSTRAINT [CHK_action] CHECK ([action] = 'update' OR [action] = 'delete')
)
GO

-- �������� ��������
CREATE TRIGGER [TRG_ActionAudit] ON [SomeDataTable]
AFTER UPDATE, DELETE
AS
BEGIN
	INSERT INTO [Audit]
	SELECT
		[int_data],
		[string_data],
		CASE 
			WHEN EXISTS (SELECT * FROM [inserted]) THEN 'update'
			ELSE 'delete'
		END AS [action],
		GETDATE() AS [date_changed],
		SYSTEM_USER AS [changed_by_user_name]
	FROM [deleted]	
END;
GO
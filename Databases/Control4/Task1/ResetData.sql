-- Очищение таблиц и ввод тестовых данных

USE [ControlDB]
GO

DELETE [dbo].[SomeDataTable]
GO

DELETE [dbo].[Audit]
GO

-- Ввод тестовых данных
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (1, N'Один');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (2, N'Два');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (3, N'Три');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (4, N'Четыре');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (5, N'Пять');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (6, N'Шесть');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (7, N'Семь');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (8, N'Восемь');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (9, N'Девять');
INSERT INTO [dbo].[SomeDataTable] ([int_data], [string_data])
VALUES (10, N'Десять');
GO